# Interval_Network

## Description
Pruning of a neural network based on significance values of the neurons. The network can not calculate the accuracy of the new network. This will be done by calling external python scripts.

## Setup
The dependencies for the interval_network library are boost, Eigen3, nlohmann_json and NAG::dco_cpp. To install the first three ones (sudo previledges required):

    sudo apt-get install libboost-all-dev libeigen3-dev nlohmann-json3-dev

With the current setup (cmake/FindNAG_dco_cpp.cmake) the dco library will be found if installed properly or if placed into a folder named extern. In the latter case the folder containing dco should just be called dco without any version number included in the directory name
