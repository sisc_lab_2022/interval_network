// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_INTEROPERABILITY_BOOST_INTERVAL

#pragma once

#include <boost/numeric/interval.hpp>
#include <boost/numeric/interval/io.hpp>
#include <dco.hpp>

namespace interval_network {

// interval definition
using interval_bt = @INTERVAL_NETWORK_INTERVAL_BASETYPE@;
using Interval = boost::numeric::interval<interval_bt,
    boost::numeric::interval_lib::policies<
        boost::numeric::interval_lib::save_state<boost::numeric::interval_lib::rounded_transc_exact<interval_bt>>,
        boost::numeric::interval_lib::checking_base<interval_bt>>>;

// dco types
using dco_mode = dco::ga1s<Interval>;
using dco_type = dco_mode::type;

} // namespace interval_network