// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include <dco.hpp>
#include <fstream>
#include <interval_network/functional_network.hpp>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <vector>

int main(int, char**)
{
    // Let's imagine the following scenario:
    // A sequential network contains the layer with the name "conv2d_1"
    // The layer is followed by the layer with name "max_pooling2d_1"
    // followed by "flatten", followed by "dense"
    // after this and before this there can be more layers, but they are not effected
    // by the pruning (in this case at least)
    // We want to prune 10 elements from "conv2d_1"

    // interval_bt can be specified in CMakeLists.txt before loading the library
    // default is double
    using interval_network::interval_bt;

    using interval_network::dco_mode;
    using interval_network::dco_type;
    using dco_ttype = dco_mode::tape_t;

    // original network
    std::string networkPath = "/path/to/simple_convnet.json";

    std::cout << "read json file" << std::endl;
    std::ifstream f(networkPath);
    nlohmann::json network_json = nlohmann::json::parse(f);
    f.close();

    std::cout << "construct network from json" << std::endl;
    interval_network::FunctionalNetwork network(network_json);

    dco_mode::global_tape = dco_ttype::create();

    // input + register input
    // a tensor 28x28x1 is filled with intervals from 0 to 1
    Eigen::Tensor<dco_type, 3> inp(28, 28, 1);
    inp.setConstant(interval_network::Interval(0, 1));

    for (int i = 0; i < inp.dimension(0); i++) {
        for (int j = 0; j < inp.dimension(1); j++) {
            for (int k = 0; k < inp.dimension(2); k++) {
                dco_mode::global_tape->register_variable(inp(i, j, k));
            }
        }
    }

    // forward pass
    std::cout << "forward pass" << std::endl;
    Eigen::Tensor<dco_type, 3> res = network.forward(inp, true);

    // register output
    for (int i = 0; i < res.dimension(0); i++) {
        for (int j = 0; j < res.dimension(1); j++) {
            for (int k = 0; k < res.dimension(2); k++) {
                dco_mode::global_tape->register_output_variable(res(i, j, k));
            }
        }
    }

    // backward pass
    int n_outputs = res.dimension(0) * res.dimension(1) * res.dimension(2);
    for (int i = 0; i < res.dimension(0); i++) {
        for (int j = 0; j < res.dimension(1); j++) {
            for (int k = 0; k < res.dimension(2); k++) {
                std::cout << "backward pass for output node " << (i + 1) * (j + 1) * (k + 1) << " out of " << n_outputs
                          << std::endl;

                // set only one direction to 1
                dco::derivative(res(i, j, k)) = 1;

                dco_mode::global_tape->interpret_adjoint();
                network.update_significances();
                dco_mode::global_tape->zero_adjoints();
            }
        }
    }

    dco_ttype::remove(dco_mode::global_tape);

    // prune 10 elements from the layer "conv_1"
    for (size_t i = 0; i < 10; i++) {
        std::vector<interval_bt> significances = network.layers["conv2d_1"]->get_significances();
        int min_idx = std::distance(
            std::begin(significances), std::min_element(std::begin(significances), std::end(significances)));

        interval_network::PruningResult pruning_result = network.layers["conv2d_1"]->prune_channel(min_idx);
        pruning_result = network.layers["max_pooling2d_1"]->previous_nodes_removed(pruning_result);
        pruning_result = network.layers["flatten"]->previous_nodes_removed(pruning_result);
        network.layers["dense"]->previous_nodes_removed(pruning_result);
    }

    // save network
    auto js = network.to_json();
    std::ofstream o("/path/to/pruned_network.json");
    o << std::setw(2) << js << std::endl;
    o.close();
}
