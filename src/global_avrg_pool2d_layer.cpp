// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include "interval_network/global_avrg_pool2d_layer.hpp"
#include "interval_network/activation_functions.hpp"
#include "interval_network/layer.hpp"
#include "interval_network_config.hpp"
#include <algorithm>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <unsupported/Eigen/CXX11/Tensor>

namespace interval_network {
GlobalAvrgPool2dLayer::GlobalAvrgPool2dLayer(
    const std::string& name, const std::string& input_node, int input_rows, int input_cols, int input_channels)
    : name_(name)
    , input_node_(input_node)
    , input_rows_(input_rows)
    , input_cols_(input_cols)
    , input_channels_(input_channels)
    , forward_called_(false)
{
    values_ = Eigen::Tensor<dco_type, 3>(1, 1, input_channels_);
    values_.setConstant(dco_type(Interval(0)));
}

GlobalAvrgPool2dLayer::GlobalAvrgPool2dLayer(const nlohmann::json& json)
{
    assert(json.at("type") == "globalavrgpool2d");
    name_ = json.at("name");

    std::vector<std::string> input_nodes = json.at("input_nodes").get<std::vector<std::string>>();
    assert(input_nodes.size() == 1);
    input_node_ = input_nodes[0];

    input_rows_ = json.at("input_shape")[0];
    input_cols_ = json.at("input_shape")[1];
    input_channels_ = json.at("input_shape")[2];

    values_ = Eigen::Tensor<dco_type, 3>(1, 1, input_channels_);
    values_.setConstant(dco_type(Interval(0)));

    forward_called_ = false;
}

Eigen::Tensor<dco_type, 3> GlobalAvrgPool2dLayer::forward(
    const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization)
{
    if (forward_called_) {
        return values_;
    }

    assert(inputs.size() == 1); // only 1 input allowed
    auto input = inputs[0];

    assert(input_rows_ == input.dimension(0));
    assert(input_cols_ == input.dimension(1));
    assert(input_channels_ == input.dimension(2));

    for (int k = 0; k < input_channels_; k++) {
        dco_type avrg_value = Interval(0);
        for (int i = 0; i < input_rows_; i++) {
            for (int j = 0; j < input_cols_; j++) {
                avrg_value += input(i, j, k);
            }
        }
        avrg_value = avrg_value / (input_rows_ * input_cols_);
        values_(0, 0, k) = avrg_value;
    }

    forward_called_ = true;

    if (apply_normalization) {
        // TODO
    }

    return values_;
}

PruningResult GlobalAvrgPool2dLayer::prune_channel(int channel_idx)
{
    std::vector<std::tuple<int, int, int>> indices_back = { std::make_tuple(0, 0, channel_idx) };
    std::vector<Interval> intervals_back = { dco::value(values_(0, 0, channel_idx)) };

    // remove obsolete values
    input_channels_ -= 1;

    auto values_new = Eigen::Tensor<dco_type, 3>(1, 1, input_channels_);

    for (int i = 0; i < channel_idx; i++) {
        values_new(0, 0, i) = values_(0, 0, i);
    }

    for (int i = channel_idx; i < input_channels_; i++) {
        values_new(0, 0, i) = values_(0, 0, i + 1);
    }

    values_ = values_new;

    return PruningResult(indices_back, intervals_back);
}

PruningResult GlobalAvrgPool2dLayer::previous_nodes_removed(const PruningResult& prev_pruning_result)
{
    if (prev_pruning_result.indices.empty()) {
        return prev_pruning_result;
    }

    // set index k to first element in list if there is a first element
    int channel_to_remove = std::get<2>(prev_pruning_result.indices[0]);

    // check if exactly one channel should be removed (only possible option so far)
    // that means if every index in the input is associated with one index in the vector
    assert(prev_pruning_result.indices.size() == input_rows_ * input_cols_);
    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            auto index = std::find(prev_pruning_result.indices.begin(), prev_pruning_result.indices.end(),
                std::make_tuple(i, j, channel_to_remove));

            // if element not in list, index will point to element after last element in list
            assert(index != prev_pruning_result.indices.end());
        }
    }

    return prune_channel(channel_to_remove);
}

std::shared_ptr<Layer> GlobalAvrgPool2dLayer::clone() const
{
    auto c = std::make_shared<GlobalAvrgPool2dLayer>(name_, input_node_, input_rows_, input_cols_, input_channels_);
    c->values_ = values_;
    c->forward_called_ = forward_called_;
    return c;
}

void GlobalAvrgPool2dLayer::print() const
{
    std::cout << "GlobalAvrgPool2D layer (" << input_rows_ << "," << input_cols_ << "," << input_channels_
              << ") -> (1,1," << input_channels_ << ") " << name_ << std::endl;
    std::cout << "\tinput node: " << input_node_ << std::endl;
}

nlohmann::json GlobalAvrgPool2dLayer::to_json() const
{
    std::vector<int> input_shape(3);
    input_shape[0] = input_rows_;
    input_shape[1] = input_cols_;
    input_shape[2] = input_channels_;

    std::vector<std::string> input_nodes = { input_node_ };

    auto j = nlohmann::json { { "name", name_ }, { "type", "globalavrgpool2d" }, { "input_shape", input_shape },
        { "input_nodes", input_nodes } };

    return j;
}
} // namespace interval_network
