// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include "interval_network/batchnormalization_layer.hpp"
#include "interval_network/activation_functions.hpp"
#include "interval_network/layer.hpp"
#include "interval_network_config.hpp"
#include <algorithm>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <tuple>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

namespace interval_network {

BatchnormalizationLayer::BatchnormalizationLayer(const std::string& name, const std::string& input_node, int input_rows,
    int input_cols, int input_channels, interval_bt momentum, interval_bt epsilon,
    const Eigen::Tensor<interval_bt, 1>& gamma, const Eigen::Tensor<interval_bt, 1>& beta,
    const Eigen::Tensor<interval_bt, 1>& mean, const Eigen::Tensor<interval_bt, 1>& variance)
    : name_(name)
    , input_node_(input_node)
    , input_rows_(input_rows)
    , input_cols_(input_cols)
    , input_channels_(input_channels)
    , momentum_(momentum)
    , epsilon_(epsilon)
    , gamma_(gamma)
    , beta_(beta)
    , mean_(mean)
    , variance_(variance)
    , forward_called_(false)
{
    values_ = Eigen::Tensor<dco_type, 3>(input_rows_, input_cols_, input_channels_);
    values_.setConstant(dco_type(Interval(0)));
}

BatchnormalizationLayer::BatchnormalizationLayer(const nlohmann::json& json)
{
    assert(json.at("type") == "batchnormalization");

    name_ = json.at("name");

    std::vector<std::string> input_nodes = json.at("input_nodes").get<std::vector<std::string>>();
    assert(input_nodes.size() == 1);

    input_node_ = input_nodes[0];
    forward_called_ = false;

    input_rows_ = json.at("input_shape")[0];
    input_cols_ = json.at("input_shape")[1];
    input_channels_ = json.at("input_shape")[2];

    momentum_ = json.at("momentum");
    epsilon_ = json.at("epsilon");

    assert(json.at("gamma").size() == input_channels_);
    assert(json.at("beta").size() == input_channels_);
    assert(json.at("mean").size() == input_channels_);
    assert(json.at("var").size() == input_channels_);

    gamma_ = Eigen::Tensor<interval_bt, 1>(input_channels_);
    beta_ = Eigen::Tensor<interval_bt, 1>(input_channels_);
    mean_ = Eigen::Tensor<interval_bt, 1>(input_channels_);
    variance_ = Eigen::Tensor<interval_bt, 1>(input_channels_);

    for (int i = 0; i < input_channels_; i++) {
        gamma_(i) = json.at("gamma")[i];
        beta_(i) = json.at("beta")[i];
        mean_(i) = json.at("mean")[i];
        variance_(i) = json.at("var")[i];
    }

    values_ = Eigen::Tensor<dco_type, 3>(input_rows_, input_cols_, input_channels_);
    values_.setConstant(dco_type(Interval(0)));
}

Eigen::Tensor<dco_type, 3> BatchnormalizationLayer::forward(
    const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization)
{
    if (forward_called_) {
        return values_;
    }

    assert(inputs.size() == 1); // only 1 input allowed
    auto input = inputs[0];

    assert(input.dimension(0) == input_rows_);
    assert(input.dimension(1) == input_cols_);
    assert(input.dimension(2) == input_channels_);

    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            for (int k = 0; k < input_channels_; k++) {
                values_(i, j, k) = beta_(k) + gamma_(k) * ((input(i, j, k) - mean_(k)) / sqrt(epsilon_ + variance_(k)));
            }
        }
    }

    forward_called_ = true;

    if (apply_normalization) {
        // TODO
    }

    return values_;
}

PruningResult BatchnormalizationLayer::prune_channel(int channel_idx)
{
    auto intervals_back = std::vector<Interval>();
    auto indices = std::vector<std::tuple<int, int, int>>();

    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            auto index = std::make_tuple(i, j, channel_idx);
            indices.push_back(index);
            intervals_back.push_back(dco::value(values_(i, j, channel_idx)));
        }
    }

    // delete obsolete elements
    input_channels_ -= 1;

    auto gamma_new = Eigen::Tensor<interval_bt, 1>(input_channels_);
    auto beta_new = Eigen::Tensor<interval_bt, 1>(input_channels_);
    auto mean_new = Eigen::Tensor<interval_bt, 1>(input_channels_);
    auto variance_new = Eigen::Tensor<interval_bt, 1>(input_channels_);
    auto values_new = Eigen::Tensor<dco_type, 3>(input_rows_, input_cols_, input_channels_);

    for (int i = 0; i < channel_idx; i++) {
        gamma_new(i) = gamma_(i);
        beta_new(i) = beta_(i);
        mean_new(i) = mean_(i);
        variance_new(i) = variance_(i);
        for (int j = 0; j < input_rows_; j++) {
            for (int k = 0; k < input_cols_; k++) {
                values_new(j, k, i) = values_(j, k, i);
            }
        }
    }

    for (int i = channel_idx; i < input_channels_; i++) {
        gamma_new(i) = gamma_(i + 1);
        beta_new(i) = beta_(i + 1);
        mean_new(i) = mean_(i + 1);
        variance_new(i) = variance_(i + 1);
        for (int j = 0; j < input_rows_; j++) {
            for (int k = 0; k < input_cols_; k++) {
                values_new(j, k, i) = values_(j, k, i + 1);
            }
        }
    }

    gamma_ = gamma_new;
    beta_ = beta_new;
    mean_ = mean_new;
    variance_ = variance_new;
    values_ = values_new;

    return PruningResult(indices, intervals_back);
}

PruningResult BatchnormalizationLayer::previous_nodes_removed(const PruningResult& prev_pruning_result)
{
    if (prev_pruning_result.indices.empty()) {
        return prev_pruning_result;
    }

    // set index k to first element in list if there is a first element
    int channel_to_remove = std::get<2>(prev_pruning_result.indices[0]);

    // check if exactly one channel should be removed (only possible option so far)
    assert(prev_pruning_result.indices.size() == input_rows_ * input_cols_);
    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            auto index = std::find(prev_pruning_result.indices.begin(), prev_pruning_result.indices.end(),
                std::make_tuple(i, j, channel_to_remove));

            // if element not in list, index will point to element after last element in list
            assert(index != prev_pruning_result.indices.end());
        }
    }

    return prune_channel(channel_to_remove);
}

nlohmann::json BatchnormalizationLayer::to_json() const
{
    auto gamma = std::vector<interval_bt>(input_channels_);
    auto beta = std::vector<interval_bt>(input_channels_);
    auto mean = std::vector<interval_bt>(input_channels_);
    auto variance = std::vector<interval_bt>(input_channels_);
    for (int i = 0; i < input_channels_; i++) {
        gamma[i] = gamma_(i);
        beta[i] = beta_(i);
        mean[i] = mean_(i);
        variance[i] = variance_(i);
    }

    std::vector<std::string> input_nodes = { input_node_ };
    std::vector<int> input_shape(3);
    input_shape[0] = input_rows_;
    input_shape[1] = input_cols_;
    input_shape[2] = input_channels_;

    auto j = nlohmann::json { { "name", name_ }, { "type", "batchnormalization" }, { "momentum", momentum_ },
        { "epsilon", epsilon_ }, { "gamma", gamma }, { "beta", beta }, { "mean", mean }, { "var", variance },
        { "input_shape", input_shape }, { "input_nodes", input_nodes } };

    return j;
}

void BatchnormalizationLayer::print() const
{
    std::cout << "Batchnormalization layer (" << input_rows_ << "," << input_cols_ << "," << input_channels_ << ") -> ("
              << input_rows_ << "," << input_cols_ << "," << input_channels_ << ") " << name_ << std::endl;
    std::cout << "\tinput node: " << input_node_ << std::endl;
}

std::shared_ptr<Layer> BatchnormalizationLayer::clone() const
{
    auto c = std::make_shared<BatchnormalizationLayer>(name_, input_node_, input_rows_, input_cols_, input_channels_,
        momentum_, epsilon_, gamma_, beta_, mean_, variance_);
    c->values_ = values_;
    c->forward_called_ = forward_called_;
    return c;
}

}
