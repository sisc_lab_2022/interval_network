// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include "interval_network/add_layer.hpp"
#include "interval_network/activation_functions.hpp"
#include "interval_network/layer.hpp"
#include "interval_network_config.hpp"
#include <algorithm>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <tuple>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

namespace interval_network {
AddLayer::AddLayer(const std::string& name, const std::vector<std::string>& input_layers, int input_rows,
    int input_cols, int input_channels)
    : name_(name)
    , input_nodes_(input_layers)
    , input_rows_(input_rows)
    , input_cols_(input_cols)
    , input_channels_(input_channels)
{
    forward_called_ = false;
    values_ = Eigen::Tensor<dco_type, 3>(input_rows_, input_cols_, input_channels_);
    values_.setConstant(dco_type(Interval(0)));
    significances_ = Eigen::Tensor<interval_bt, 3>(input_rows_, input_cols_, input_channels_);
    significances_.setConstant(0);
}

AddLayer::AddLayer(const nlohmann::json& json)
{
    assert(json.at("type") == "add");

    name_ = json.at("name");
    input_nodes_ = json.at("input_nodes").get<std::vector<std::string>>();

    input_rows_ = json.at("input_shape")[0];
    input_cols_ = json.at("input_shape")[1];
    input_channels_ = json.at("input_shape")[2];

    forward_called_ = false;
    values_ = Eigen::Tensor<dco_type, 3>(input_rows_, input_cols_, input_channels_);
    values_.setConstant(dco_type(Interval(0)));
    significances_ = Eigen::Tensor<interval_bt, 3>(input_rows_, input_cols_, input_channels_);
    significances_.setConstant(0);
}

Eigen::Tensor<dco_type, 3> AddLayer::forward(
    const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization)
{
    if (forward_called_) {
        return values_;
    }

    for (const auto& input : inputs) {
        assert(input.dimension(0) == input_rows_);
        assert(input.dimension(1) == input_cols_);
        assert(input.dimension(2) == input_channels_);
    }

    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            for (int k = 0; k < input_channels_; k++) {
                values_(i, j, k) = 0;
                for (const auto& input : inputs) {
                    values_(i, j, k) += input(i, j, k);
                }
            }
        }
    }

    forward_called_ = true;

    if (apply_normalization) {
        // TODO
    }

    return values_;
}

void AddLayer::update_significance()
{
    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            for (int k = 0; k < input_channels_; k++) {
                interval_bt width = dco::value(values_(i, j, k)).upper() - dco::value(values_(i, j, k)).lower();
                interval_bt abs_derivative_up = abs(dco::derivative(values_(i, j, k)).upper());
                interval_bt abs_derivative_low = abs(dco::derivative(values_(i, j, k)).lower());
                interval_bt abs_derivative = std::max(abs_derivative_up, abs_derivative_low);
                interval_bt significance = width * abs_derivative;
                // interval_bt significance = width;
                significances_(i, j, k) = std::max(significances_(i, j, k), significance);
            }
        }
    }
}

std::vector<interval_bt> AddLayer::get_significances() const
{
    std::vector<interval_bt> significaces(input_channels_);
    for (int k = 0; k < input_channels_; k++) {
        interval_bt sum = 0;
        for (int i = 0; i < input_rows_; i++) {
            for (int j = 0; j < input_cols_; j++) {
                sum += significances_(i, j, k);
            }
        }
        significaces[k] = sum;
    }
    return significaces;
}

PruningResult AddLayer::prune_channel(int channel_idx)
{
    auto intervals = std::vector<Interval>();
    auto indices = std::vector<std::tuple<int, int, int>>();

    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            auto index = std::make_tuple(i, j, channel_idx);
            indices.push_back(index);
            intervals.push_back(dco::value(values_(i, j, channel_idx)));
        }
    }

    input_channels_ -= 1;

    // remove obsolete values
    auto values_new = Eigen::Tensor<dco_type, 3>(input_rows_, input_cols_, input_channels_);

    for (int i = 0; i < channel_idx; i++) {
        for (int j = 0; j < input_rows_; j++) {
            for (int k = 0; k < input_cols_; k++) {
                values_new(j, k, i) = values_(j, k, i);
            }
        }
    }

    for (int i = channel_idx; i < input_channels_; i++) {
        for (int j = 0; j < input_rows_; j++) {
            for (int k = 0; k < input_cols_; k++) {
                values_new(j, k, i) = values_(j, k, i + 1);
            }
        }
    }

    // delete unnecessary significance
    Eigen::Tensor<interval_bt, 3> new_significances(input_rows_, input_cols_, input_channels_);
    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            // first half until index to remove
            for (int k = 0; k < channel_idx; k++) {
                new_significances(i, j, k) = significances_(i, j, k);
            }
            // second half (skipping index to remove)
            for (int k = channel_idx; k < input_channels_; k++) {
                new_significances(i, j, k) = significances_(i, j, k + 1);
            }
        }
    }
    significances_ = new_significances;

    return PruningResult(indices, intervals);
}

PruningResult AddLayer::previous_nodes_removed(const PruningResult& prev_pruning_result)
{
    if (prev_pruning_result.indices.empty()) {
        return prev_pruning_result;
    }

    // set index k to first element in list if there is a first element
    int channel_to_remove = std::get<2>(prev_pruning_result.indices[0]);

    // check if exactly one channel should be removed (only possible option so far)
    // that means if every index in the input is associated with one index in the vector
    assert(prev_pruning_result.indices.size() == input_rows_ * input_cols_);
    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            auto index = std::find(prev_pruning_result.indices.begin(), prev_pruning_result.indices.end(),
                std::make_tuple(i, j, channel_to_remove));

            // if element not in list, index will point to element after last element in list
            assert(index != prev_pruning_result.indices.end());
        }
    }

    return prune_channel(channel_to_remove);
}

nlohmann::json AddLayer::to_json() const
{
    std::vector<int> input_shape(3);
    input_shape[0] = input_rows_;
    input_shape[1] = input_cols_;
    input_shape[2] = input_channels_;

    nlohmann::json j
        = { { "name", name_ }, { "type", "add" }, { "input_shape", input_shape }, { "input_nodes", input_nodes_ } };

    return j;
}

void AddLayer::print() const
{
    std::cout << "Add layer (" << input_rows_ << "," << input_cols_ << "," << input_channels_ << ") -> (" << input_rows_
              << "," << input_cols_ << "," << input_channels_ << ") " << name_ << std::endl;
    std::cout << "\tinput nodes: ";
    for (const auto& input_node : input_nodes_) {
        std::cout << input_node << " ";
    }
    std::cout << std::endl;
}

std::shared_ptr<Layer> AddLayer::clone() const
{
    auto c = std::make_shared<AddLayer>(name_, input_nodes_, input_rows_, input_cols_, input_channels_);
    c->values_ = values_;
    c->significances_ = significances_;
    c->forward_called_ = forward_called_;
    return c;
}
} // namespace interval_network
