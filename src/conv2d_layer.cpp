// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include "interval_network/conv2d_layer.hpp"
#include "interval_network/activation_functions.hpp"
#include "interval_network/layer.hpp"
#include "interval_network_config.hpp"
#include <algorithm>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <tuple>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

namespace interval_network {
Conv2dLayer::Conv2dLayer(const std::string& name, const std::string& input_node,
    const Eigen::Tensor<interval_bt, 4>& kernel, int input_rows, int input_cols, std::string padding, int stride_rows,
    int stride_cols, const Eigen::Tensor<interval_bt, 1>& biases, const std::shared_ptr<ActivationFunction>& f)
    : name_(name)
    , input_node_(input_node)
    , kernel_(kernel)
    , input_rows_(input_rows)
    , input_cols_(input_cols)
    , stride_rows_(stride_rows)
    , stride_cols_(stride_cols)
    , biases_(biases)
    , activation_(f)
    , forward_called_(false)
{

    int kernel_rows = kernel.dimension(0);
    int kernel_cols = kernel.dimension(1);
    input_channels_ = kernel.dimension(2);
    output_channels_ = kernel.dimension(3);
    // Compute the shape of the output tensor
    if (padding == "valid") {
        output_rows_ = (input_rows - kernel_rows + stride_rows) / stride_rows;
        output_cols_ = (input_cols - kernel_cols + stride_cols) / stride_cols;
        padding_rows_ = 0;
        padding_cols_ = 0;
    } else if (padding == "same") {
        // tensorflows implementation for this case has a strange behaviour that we did not understand.
        // Therefore combining padding with stride is not allowed here. It's not a unseful thing anyways
        assert(stride_rows == 1);
        assert(stride_cols == 1);

        output_rows_ = (input_rows + stride_rows - 1) / stride_rows;
        output_cols_ = (input_cols + stride_cols - 1) / stride_cols;
        padding_rows_ = kernel_rows - 1;
        padding_cols_ = kernel_cols - 1;
    } else {
        throw std::invalid_argument("Invalid padding type");
    }
    // Initialize output tensor and significances with zeros
    values_ = Eigen::Tensor<dco_type, 3>(output_rows_, output_cols_, output_channels_);
    values_.setConstant(dco_type(Interval(0)));

    significances_ = Eigen::Tensor<interval_bt, 3>(output_rows_, output_cols_, output_channels_);
    significances_.setConstant(0);
}

Conv2dLayer::Conv2dLayer(const nlohmann::json& json)
{
    assert(json.at("type") == "conv2d");

    name_ = json.at("name");

    std::vector<std::string> input_nodes = json.at("input_nodes").get<std::vector<std::string>>();
    assert(input_nodes.size() == 1);

    input_node_ = input_nodes[0];

    // kernel
    // conversion to long to avoid compiler warning (narrowing)
    kernel_ = Eigen::Tensor<interval_bt, 4>((long)json.at("weights").size(), (long)json.at("weights")[0].size(),
        (long)json.at("weights")[0][0].size(), (long)json.at("weights")[0][0][0].size());
    for (int i = 0; i < kernel_.dimension(0); i++) {
        for (int j = 0; j < kernel_.dimension(1); j++) {
            for (int k = 0; k < kernel_.dimension(2); k++) {
                for (int l = 0; l < kernel_.dimension(3); l++) {
                    kernel_(i, j, k, l) = json.at("weights")[i][j][k][l];
                }
            }
        }
    }
    int kernel_rows = kernel_.dimension(0);
    int kernel_cols = kernel_.dimension(1);
    input_channels_ = kernel_.dimension(2);
    output_channels_ = kernel_.dimension(3);
    // biases
    biases_ = Eigen::Tensor<interval_bt, 1>(json.at("biases").size());
    for (int i = 0; i < biases_.dimension(0); i++) {
        biases_(i) = json.at("biases")[i];
    }

    input_rows_ = json.at("input_shape")[0];
    input_cols_ = json.at("input_shape")[1];
    stride_rows_ = json.at("strides")[0];
    stride_cols_ = json.at("strides")[1];

    std::string padding = json.at("padding");
    // Compute the shape of the output tensor
    if (padding == "valid") {
        output_rows_ = (input_rows_ - kernel_rows + stride_rows_) / stride_rows_;
        output_cols_ = (input_cols_ - kernel_cols + stride_cols_) / stride_cols_;
        padding_rows_ = 0;
        padding_cols_ = 0;
    } else if (padding == "same") {
        // tensorflows implementation for this case has a strange behaviour that we did not understand.
        // Therefore combining padding with stride is not allowed here. It's not a unseful thing anyways
        assert(stride_rows_ == 1);
        assert(stride_cols_ == 1);

        output_rows_ = (input_rows_ + stride_rows_ - 1) / stride_rows_;
        output_cols_ = (input_cols_ + stride_cols_ - 1) / stride_cols_;
        padding_rows_ = kernel_rows - 1;
        padding_cols_ = kernel_cols - 1;
    } else {
        throw std::invalid_argument("Invalid padding type");
    }

    activation_ = activation_from_json(json.at("activation"));

    // Initialize output tensor and significances with zeros
    values_ = Eigen::Tensor<dco_type, 3>(output_rows_, output_cols_, output_channels_);
    values_.setConstant(dco_type(Interval(0)));

    significances_ = Eigen::Tensor<interval_bt, 3>(output_rows_, output_cols_, output_channels_);
    significances_.setConstant(0);

    forward_called_ = false;
}

Eigen::Tensor<dco_type, 3> Conv2dLayer::forward(
    const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization)
{
    if (forward_called_) {
        return values_;
    }

    assert(inputs.size() == 1); // only 1 input allowed
    auto input = inputs[0];

    assert(input.dimension(0) == input_rows_);
    assert(input.dimension(1) == input_cols_);
    assert(input.dimension(2) == input_channels_);

    // Add padding to the input tensor if necessary
    Eigen::Tensor<dco_type, 3> padded_input;
    if (padding_rows_ > 0 || padding_cols_ > 0) {
        padded_input = Eigen::Tensor<dco_type, 3>(
            input_rows_ + 2 * padding_rows_, input_cols_ + 2 * padding_cols_, input_channels_);
        padded_input.setConstant(dco_type(Interval(0)));

        for (int i = 0; i < input_rows_; i++) {
            for (int j = 0; j < input_cols_; j++) {
                for (int k = 0; k < input_channels_; k++) {
                    padded_input(i + padding_rows_ / 2, j + padding_cols_ / 2, k) = input(i, j, k);
                }
            }
        }
    } else {
        padded_input = input;
    }

    for (int i = 0; i < output_rows_; i++) {
        for (int j = 0; j < output_cols_; j++) {
            for (int k = 0; k < output_channels_; k++) {
                // Initialize the sum for this output element
                dco_type sum = dco_type(Interval(0));
                for (int l = 0; l < input_channels_; l++) {
                    // Loop over the rows and columns of the kernel
                    for (int m = 0; m < kernel_.dimension(0); m++) {
                        for (int n = 0; n < kernel_.dimension(1); n++) {
                            // Accumulate the element-wise product of the kernel and the input
                            sum += kernel_(m, n, l, k) * padded_input(i * stride_rows_ + m, j * stride_cols_ + n, l);
                        }
                    }
                }
                // Set the output element to the sum
                values_(i, j, k) = sum;
            }
        }
    }

    // bias
    for (int i = 0; i < output_rows_; i++) {
        for (int j = 0; j < output_cols_; j++) {
            for (int k = 0; k < output_channels_; k++) {
                values_(i, j, k) += biases_(k);
            }
        }
    }

    // activation function
    values_ = activation_->eval(values_);
    forward_called_ = true;

    if (apply_normalization) {
        // TODO
    }

    return values_;
}

void Conv2dLayer::update_significance()
{
    for (int i = 0; i < output_rows_; i++) {
        for (int j = 0; j < output_cols_; j++) {
            for (int k = 0; k < output_channels_; k++) {
                interval_bt width = dco::value(values_(i, j, k)).upper() - dco::value(values_(i, j, k)).lower();
                interval_bt abs_derivative_up = abs(dco::derivative(values_(i, j, k)).upper());
                interval_bt abs_derivative_low = abs(dco::derivative(values_(i, j, k)).lower());
                interval_bt abs_derivative = std::max(abs_derivative_up, abs_derivative_low);
                interval_bt significance = width * abs_derivative;
                // interval_bt significance = width;
                significances_(i, j, k) = std::max(significances_(i, j, k), significance);
            }
        }
    }
}

std::vector<interval_bt> Conv2dLayer::get_significances() const
{
    std::vector<interval_bt> significaces(output_channels_);
    for (int k = 0; k < output_channels_; k++) {
        interval_bt sum = 0;
        for (int i = 0; i < output_rows_; i++) {
            for (int j = 0; j < output_cols_; j++) {
                sum += significances_(i, j, k);
            }
        }
        significaces[k] = sum;
    }
    return significaces;
}

void Conv2dLayer::print() const
{
    // std::vector<interval_bt> significances = this->getSignificances();

    std::cout << "Convolutional2D layer (" << input_rows_ << "," << input_cols_ << "," << input_channels_ << ") -> ("
              << output_rows_ << "," << output_cols_ << "," << output_channels_ << ") " << name_ << std::endl;
    std::cout << "\tstrides: (" << stride_rows_ << "," << stride_cols_ << ")" << std::endl;
    std::cout << "\tactivation function: " << activation_->name() << std::endl;
    std::cout << "\tinput node: " << input_node_ << std::endl;
    // std::cout << "\tsignificances: " << significances.reshape(Eigen::array<int, 2>{{1,
    // (int)significances.dimension(0)}}) << std::endl;
}

PruningResult Conv2dLayer::prune_channel(int channel_idx)
{
    assert(channel_idx < output_channels_);

    std::vector<Interval> intervals_back;
    std::vector<std::tuple<int, int, int>> pruned_indices;

    for (int i = 0; i < output_rows_; i++) {
        for (int j = 0; j < output_cols_; j++) {
            intervals_back.push_back(dco::value(values_(i, j, channel_idx)));
            pruned_indices.push_back(std::make_tuple(i, j, channel_idx));
        }
    }

    // update number of output channels
    output_channels_ -= 1;

    // delete output channel from kernel
    Eigen::Tensor<interval_bt, 4> new_kernel(
        kernel_.dimension(0), kernel_.dimension(1), input_channels_, output_channels_);
    for (int i = 0; i < new_kernel.dimension(0); i++) {
        for (int j = 0; j < new_kernel.dimension(1); j++) {
            for (int k = 0; k < input_channels_; k++) {
                // first half of the kernel until index to remove
                for (int l = 0; l < channel_idx; l++) {
                    new_kernel(i, j, k, l) = kernel_(i, j, k, l);
                }

                // second half (skipping index to remove)
                for (int l = channel_idx; l < output_channels_; l++) {
                    new_kernel(i, j, k, l) = kernel_(i, j, k, l + 1);
                }
            }
        }
    }
    kernel_ = new_kernel;

    // delete unnecessary bias
    Eigen::Tensor<interval_bt, 1> new_biases(output_channels_);
    // first half of the bias vector until index to remove
    for (int i = 0; i < channel_idx; i++) {
        new_biases(i) = biases_(i);
    }
    // second half (skipping index to remove)
    for (int i = channel_idx; i < output_channels_; i++) {
        new_biases(i) = biases_(i + 1);
    }
    biases_ = new_biases;

    // delete unnecessary values
    auto values_new = Eigen::Tensor<dco_type, 3>(output_rows_, output_cols_, output_channels_);

    for (int i = 0; i < channel_idx; i++) {
        for (int j = 0; j < output_rows_; j++) {
            for (int k = 0; k < output_cols_; k++) {
                values_new(j, k, i) = values_(j, k, i);
            }
        }
    }

    for (int i = channel_idx; i < output_channels_; i++) {
        for (int j = 0; j < output_rows_; j++) {
            for (int k = 0; k < output_cols_; k++) {
                values_new(j, k, i) = values_(j, k, i + 1);
            }
        }
    }

    values_ = values_new;

    // delete unnecessary significance
    Eigen::Tensor<interval_bt, 3> new_significances(output_rows_, output_cols_, output_channels_);
    for (int i = 0; i < output_rows_; i++) {
        for (int j = 0; j < output_cols_; j++) {
            // first half until index to remove
            for (int k = 0; k < channel_idx; k++) {
                new_significances(i, j, k) = significances_(i, j, k);
            }
            // second half (skipping index to remove)
            for (int k = channel_idx; k < output_channels_; k++) {
                new_significances(i, j, k) = significances_(i, j, k + 1);
            }
        }
    }
    significances_ = new_significances;

    return PruningResult(pruned_indices, intervals_back);
}

// TODO: intervalMidpoints not doing anything yet
void Conv2dLayer::previousChannelRemoved(int channel_idx, const std::vector<Interval>& intervals)
{
    assert(channel_idx < input_channels_);

    // update number of input channels
    input_channels_ -= 1;

    // delete input channel from kernel
    Eigen::Tensor<interval_bt, 4> new_kernel(
        kernel_.dimension(0), kernel_.dimension(1), input_channels_, output_channels_);
    for (int i = 0; i < new_kernel.dimension(0); i++) {
        for (int j = 0; j < new_kernel.dimension(1); j++) {
            for (int l = 0; l < output_channels_; l++) {
                // first half of the kernel until index to remove
                for (int k = 0; k < channel_idx; k++) {
                    new_kernel(i, j, k, l) = kernel_(i, j, k, l);
                }

                // second half (skipping index to remove)
                for (int k = channel_idx; k < input_channels_; k++) {
                    new_kernel(i, j, k, l) = kernel_(i, j, k + 1, l);
                }
            }
        }
    }
    kernel_ = new_kernel;
}

PruningResult Conv2dLayer::previous_nodes_removed(const PruningResult& prev_pruning_result)
{
    auto indices_back = std::vector<std::tuple<int, int, int>>();
    auto intervals_back = std::vector<Interval>();

    if (prev_pruning_result.indices.empty()) {
        return prev_pruning_result;
    }

    // set index k to first element in list if there is a first element
    int channel_to_remove = std::get<2>(prev_pruning_result.indices[0]);

    // check if exactly one channel should be removed (only possible option so far)
    assert(prev_pruning_result.indices.size() == input_rows_ * input_cols_);
    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            auto index = std::find(prev_pruning_result.indices.begin(), prev_pruning_result.indices.end(),
                std::make_tuple(i, j, channel_to_remove));

            // if element not in list, index will point to element after last element in list
            assert(index != prev_pruning_result.indices.end());
        }
    }

    previousChannelRemoved(channel_to_remove, prev_pruning_result.intervals);

    return PruningResult(indices_back, intervals_back);
}

nlohmann::json Conv2dLayer::to_json() const
{
    auto weights = std::vector<std::vector<std::vector<std::vector<interval_bt>>>>(kernel_.dimension(0),
        std::vector<std::vector<std::vector<interval_bt>>>(kernel_.dimension(1),
            std::vector<std::vector<interval_bt>>(
                kernel_.dimension(2), std::vector<interval_bt>(kernel_.dimension(3)))));

    for (int i = 0; i < kernel_.dimension(0); i++) {
        for (int j = 0; j < kernel_.dimension(1); j++) {
            for (int k = 0; k < kernel_.dimension(2); k++) {
                for (int l = 0; l < kernel_.dimension(3); l++) {
                    weights[i][j][k][l] = kernel_(i, j, k, l);
                }
            }
        }
    }

    auto biases = std::vector<interval_bt>(biases_.dimension(0));
    for (int i = 0; i < biases_.dimension(0); i++) {
        biases[i] = biases_(i);
    }

    nlohmann::json activation = activation_->to_json();

    std::vector<int> input_shape(3);
    input_shape[0] = input_rows_;
    input_shape[1] = input_cols_;
    input_shape[2] = input_channels_;

    std::string padding;
    if (padding_rows_ == 0 || padding_cols_ == 0) {
        padding = "valid";
    } else {
        padding = "same";
    }

    std::vector<int> strides(2);
    strides[0] = stride_rows_;
    strides[1] = stride_cols_;
    std::vector<std::string> input_nodes = { input_node_ };

    auto j = nlohmann::json { { "name", name_ }, { "type", "conv2d" }, { "weights", weights }, { "biases", biases },
        { "activation", activation }, { "input_shape", input_shape }, { "padding", padding }, { "strides", strides },
        { "input_nodes", input_nodes } };

    return j;
}

std::shared_ptr<Layer> Conv2dLayer::clone() const
{
    auto activationClone = activation_->clone();

    std::string padding;
    if (padding_rows_ == 0 && padding_cols_ == 0) {
        padding = "valid";
    } else {
        padding = "same";
    }

    auto c = std::make_shared<Conv2dLayer>(name_, input_node_, kernel_, input_rows_, input_cols_, padding, stride_rows_,
        stride_cols_, biases_, activationClone);

    c->values_ = values_;
    c->significances_ = significances_;
    c->forward_called_ = forward_called_;
    return c;
}
} // namespace interval_network