// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include "interval_network/activation_layer.hpp"
#include "interval_network/activation_functions.hpp"
#include "interval_network/layer.hpp"
#include "interval_network_config.hpp"
#include <algorithm>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <tuple>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

namespace interval_network {
ActivationLayer::ActivationLayer(const std::string& name, const std::string& input_node, int input_rows, int input_cols,
    int input_channels, const std::shared_ptr<ActivationFunction>& f)
    : name_(name)
    , input_node_(input_node)
    , input_rows_(input_rows)
    , input_cols_(input_cols)
    , input_channels_(input_channels)
    , activation_(f)
{
    forward_called_ = false;
    values_ = Eigen::Tensor<dco_type, 3>(input_rows_, input_cols_, input_channels_);
    values_.setConstant(dco_type(Interval(0)));
}

ActivationLayer::ActivationLayer(const nlohmann::json& json)
{
    assert(json.at("type") == "activation");

    name_ = json.at("name");

    std::vector<std::string> input_nodes = json.at("input_nodes").get<std::vector<std::string>>();
    assert(input_nodes.size() == 1);

    input_node_ = input_nodes[0];

    input_rows_ = json.at("input_shape")[0];
    input_cols_ = json.at("input_shape")[1];
    input_channels_ = json.at("input_shape")[2];

    // activation function
    activation_ = activation_from_json(json.at("activation"));

    forward_called_ = false;
    values_ = Eigen::Tensor<dco_type, 3>(input_rows_, input_cols_, input_channels_);
    values_.setConstant(dco_type(Interval(0)));
}

Eigen::Tensor<dco_type, 3> ActivationLayer::forward(
    const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization)
{
    if (forward_called_) {
        return values_;
    }

    assert(inputs.size() == 1); // only 1 input allowed
    auto input = inputs[0];

    assert(input.dimension(0) == input_rows_);
    assert(input.dimension(1) == input_cols_);
    assert(input.dimension(2) == input_channels_);

    values_ = activation_->eval(input);
    forward_called_ = true;

    if (apply_normalization) {
        // TODO
    }

    return values_;
}

PruningResult ActivationLayer::prune_channel(int channel_idx)
{
    auto intervals_back = std::vector<Interval>();
    auto indices = std::vector<std::tuple<int, int, int>>();

    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            auto index = std::make_tuple(i, j, channel_idx);
            indices.push_back(index);
            intervals_back.push_back(dco::value(values_(i, j, channel_idx)));
        }
    }

    input_channels_ -= 1;

    // remove obsolete values
    auto values_new = Eigen::Tensor<dco_type, 3>(input_rows_, input_cols_, input_channels_);

    for (int i = 0; i < channel_idx; i++) {
        for (int j = 0; j < input_rows_; j++) {
            for (int k = 0; k < input_cols_; k++) {
                values_new(j, k, i) = values_(j, k, i);
            }
        }
    }

    for (int i = channel_idx; i < input_channels_; i++) {
        for (int j = 0; j < input_rows_; j++) {
            for (int k = 0; k < input_cols_; k++) {
                values_new(j, k, i) = values_(j, k, i + 1);
            }
        }
    }

    return PruningResult(indices, intervals_back);
}

PruningResult ActivationLayer::previous_nodes_removed(const PruningResult& prev_pruning_result)
{
    if (prev_pruning_result.indices.empty()) {
        return prev_pruning_result;
    }

    // set index k to first element in list if there is a first element
    int channel_to_remove = std::get<2>(prev_pruning_result.indices[0]);

    // check if exactly one channel should be removed (only possible option so far)
    // that means if every index in the input is associated with one index in the vector
    assert(prev_pruning_result.indices.size() == input_rows_ * input_cols_);
    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            auto index = std::find(prev_pruning_result.indices.begin(), prev_pruning_result.indices.end(),
                std::make_tuple(i, j, channel_to_remove));

            // if element not in list, index will point to element after last element in list
            assert(index != prev_pruning_result.indices.end());
        }
    }

    return prune_channel(channel_to_remove);
}

nlohmann::json ActivationLayer::to_json() const
{
    std::vector<int> input_shape(3);
    input_shape[0] = input_rows_;
    input_shape[1] = input_cols_;
    input_shape[2] = input_channels_;
    std::vector<std::string> input_nodes = { input_node_ };

    nlohmann::json activation = activation_->to_json();

    nlohmann::json j = { { "name", name_ }, { "type", "activation" }, { "input_shape", input_shape },
        { "activation", activation }, { "input_nodes", input_nodes } };

    return j;
}

void ActivationLayer::print() const
{
    std::cout << "Activation layer (" << input_rows_ << "," << input_cols_ << "," << input_channels_ << ") -> ("
              << input_rows_ << "," << input_cols_ << "," << input_channels_ << ") " << name_ << std::endl;
    std::cout << "\tactivation function: " << activation_->name() << std::endl;
    std::cout << "\tinput node: " << input_node_ << std::endl;
}

std::shared_ptr<Layer> ActivationLayer::clone() const
{
    auto activation_clone = activation_->clone();

    auto c = std::make_shared<ActivationLayer>(
        name_, input_node_, input_rows_, input_cols_, input_channels_, activation_clone);
    c->values_ = values_;
    c->forward_called_ = forward_called_;
    return c;
}

} // namespace interval_network
