// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include "interval_network/flatten_layer.hpp"
#include "interval_network/layer.hpp"
#include "interval_network_config.hpp"
#include <cassert>
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <string>
#include <tuple>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

namespace interval_network {
FlattenLayer::FlattenLayer(
    const std::string& name, const std::string& input_node, int input_rows, int input_cols, int input_channels)
    : name_(name)
    , input_node_(input_node)
    , input_rows_(input_rows)
    , input_cols_(input_cols)
    , input_channels_(input_channels)
{
}

FlattenLayer::FlattenLayer(const nlohmann::json& json)
{
    assert(json.at("type") == "flatten");
    name_ = json.at("name");

    std::vector<std::string> input_nodes = json.at("input_nodes").get<std::vector<std::string>>();
    assert(input_nodes.size() == 1);
    input_node_ = input_nodes[0];

    input_rows_ = json.at("input_shape")[0];
    input_cols_ = json.at("input_shape")[1];
    input_channels_ = json.at("input_shape")[2];
}

std::shared_ptr<Layer> FlattenLayer::clone() const
{
    std::shared_ptr<Layer> c
        = std::make_shared<FlattenLayer>(name_, input_node_, input_rows_, input_cols_, input_channels_);
    return c;
}
Eigen::Tensor<dco_type, 3> FlattenLayer::forward(
    const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization)
{
    assert(inputs.size() == 1); // only 1 input allowed
    auto input = inputs[0];

    assert(input.dimension(0) == input_rows_);
    assert(input.dimension(1) == input_cols_);
    assert(input.dimension(2) == input_channels_);

    Eigen::array<int, 3> shuffle_order { { 2, 1, 0 } };
    Eigen::Tensor<dco_type, 3> input_transposed = input.shuffle(shuffle_order);

    int size_after_flatten = input.dimension(0) * input.dimension(1) * input.dimension(2);
    Eigen::array<int, 3> new_shape { { 1, 1, size_after_flatten } };
    Eigen::Tensor<dco_type, 3> output = input_transposed.reshape(new_shape);
    return output;
}

PruningResult FlattenLayer::previous_nodes_removed(const PruningResult& prev_pruning_result)
{
    if (prev_pruning_result.indices.empty()) {
        return prev_pruning_result;
    }

    int channel_to_remove = std::get<2>(prev_pruning_result.indices[0]);

    // check if exactly one channel should be removed (only possible option so far)
    // that means if every index in the input is associated with one index in the vector
    assert(prev_pruning_result.indices.size() == input_rows_ * input_cols_);
    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            auto index = std::find(prev_pruning_result.indices.begin(), prev_pruning_result.indices.end(),
                std::make_tuple(i, j, channel_to_remove));

            // if element not in list, index will point to element after last element in list
            assert(index != prev_pruning_result.indices.end());
        }
    }

    Eigen::Tensor<bool, 3> input_mask(input_rows_, input_cols_, input_channels_);
    input_mask.setConstant(false);
    Eigen::Tensor<Interval, 3> input_intervals(input_rows_, input_cols_, input_channels_);

    for (int i = 0; i < prev_pruning_result.indices.size(); i++) {
        int row = std::get<0>(prev_pruning_result.indices[i]);
        int col = std::get<1>(prev_pruning_result.indices[i]);
        int channel = std::get<2>(prev_pruning_result.indices[i]);

        input_mask(row, col, channel) = true;
        input_intervals(row, col, channel) = prev_pruning_result.intervals[i];
    }

    Eigen::array<int, 3> shuffle_order { { 2, 1, 0 } };
    Eigen::Tensor<bool, 3> input_mask_transposed = input_mask.shuffle(shuffle_order);
    Eigen::Tensor<Interval, 3> input_intervals_transposed = input_intervals.shuffle(shuffle_order);

    int size_after_flatten = input_rows_ * input_cols_ * input_channels_;
    Eigen::array<int, 3> new_shape { { 1, 1, size_after_flatten } };
    Eigen::Tensor<bool, 3> output_mask = input_mask_transposed.reshape(new_shape);
    Eigen::Tensor<Interval, 3> output_intervals = input_intervals_transposed.reshape(new_shape);

    auto indices_back = std::vector<std::tuple<int, int, int>>();
    auto intervals_back = std::vector<Interval>();

    for (int i = 0; i < size_after_flatten; i++) {
        if (output_mask(0, 0, i)) {
            indices_back.push_back(std::make_tuple(0, 0, i));
            intervals_back.push_back(output_intervals(0, 0, i));
        }
    }

    input_channels_ -= 1;

    return PruningResult(indices_back, intervals_back);
}

void FlattenLayer::print() const
{
    std::cout << "Flatten layer (" << input_rows_ << "," << input_cols_ << "," << input_channels_ << ") -> (1,1,"
              << input_rows_ * input_cols_ * input_channels_ << ") " << name_ << std::endl;
    std::cout << "\tinput node: " << input_node_ << std::endl;
}

nlohmann::json FlattenLayer::to_json() const
{
    std::vector<int> input_shape(3);
    input_shape[0] = input_rows_;
    input_shape[1] = input_cols_;
    input_shape[2] = input_channels_;
    std::vector<std::string> input_nodes = { input_node_ };

    nlohmann::json j
        = { { "name", name_ }, { "type", "flatten" }, { "input_shape", input_shape }, { "input_nodes", input_nodes } };

    return j;
}
} // namespace interval_network
