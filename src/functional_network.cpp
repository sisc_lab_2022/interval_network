// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include "interval_network/functional_network.hpp"
#include "interval_network/activation_layer.hpp"
#include "interval_network/add_layer.hpp"
#include "interval_network/avrg_pool2d_layer.hpp"
#include "interval_network/batchnormalization_layer.hpp"
#include "interval_network/conv2d_layer.hpp"
#include "interval_network/dense_layer.hpp"
#include "interval_network/flatten_layer.hpp"
#include "interval_network/global_avrg_pool2d_layer.hpp"
#include "interval_network/input_layer.hpp"
#include "interval_network/layer.hpp"
#include "interval_network/max_pool2d_layer.hpp"
#include "interval_network/zero_padding2d_layer.hpp"
#include "interval_network_config.hpp"
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <unordered_map>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

namespace interval_network {
FunctionalNetwork::FunctionalNetwork(const nlohmann::json& j)
{
    assert(j.at("type") == "functional");

    input_layer_name_ = j.at("input_layer");
    output_layer_name_ = j.at("output_layer");

    auto layers_json = j.at("layers");

    for (const nlohmann::json& layer_json : layers_json) {
        if (layer_json.at("type") == "input") {
            auto layer = std::make_shared<InputLayer>(layer_json);
            layers[layer->get_name()] = layer;
        } else if (layer_json.at("type") == "dense") {
            auto layer = std::make_shared<DenseLayer>(layer_json);
            layers[layer->get_name()] = layer;
        } else if (layer_json.at("type") == "conv2d") {
            auto layer = std::make_shared<Conv2dLayer>(layer_json);
            layers[layer->get_name()] = layer;
        } else if (layer_json.at("type") == "maxpool2d") {
            auto layer = std::make_shared<MaxPool2dLayer>(layer_json);
            layers[layer->get_name()] = layer;
        } else if (layer_json.at("type") == "avrgpool2d") {
            auto layer = std::make_shared<AvrgPool2dLayer>(layer_json);
            layers[layer->get_name()] = layer;
        } else if (layer_json.at("type") == "globalavrgpool2d") {
            auto layer = std::make_shared<GlobalAvrgPool2dLayer>(layer_json);
            layers[layer->get_name()] = layer;
        } else if (layer_json.at("type") == "zeropadding2d") {
            std::shared_ptr<Layer> layer = std::make_shared<ZeroPadding2DLayer>(layer_json);
            layers[layer->get_name()] = layer;
        } else if (layer_json.at("type") == "flatten") {
            std::shared_ptr<Layer> layer = std::make_shared<FlattenLayer>(layer_json);
            layers[layer->get_name()] = layer;
        } else if (layer_json.at("type") == "activation") {
            std::shared_ptr<Layer> layer = std::make_shared<ActivationLayer>(layer_json);
            layers[layer->get_name()] = layer;
        } else if (layer_json.at("type") == "batchnormalization") {
            std::shared_ptr<Layer> layer = std::make_shared<BatchnormalizationLayer>(layer_json);
            layers[layer->get_name()] = layer;
        } else if (layer_json.at("type") == "add") {
            std::shared_ptr<Layer> layer = std::make_shared<AddLayer>(layer_json);
            layers[layer->get_name()] = layer;
        } else {
            throw std::invalid_argument("Unknown layer type " + layer_json.at("type").get<std::string>());
        }
    }
}

void FunctionalNetwork::update_significances()
{
    for (auto& layer : layers) {
        std::get<1>(layer)->update_significance();
    }
}

Eigen::Tensor<dco_type, 3> FunctionalNetwork::forward(const Eigen::Tensor<dco_type, 3>& input, bool apply_normalization)
{
    for (auto& layer : layers) {
        std::get<1>(layer)->reset_forward_called();
    }
    return forward(input, output_layer_name_, apply_normalization);
}

Eigen::Tensor<dco_type, 3> FunctionalNetwork::forward(
    const Eigen::Tensor<dco_type, 3>& input, const std::string& layer_name, bool apply_normalization)
{
    if (layers[layer_name]->get_forward_called()) {
        return layers[layer_name]->get_values();
    }
    if (layer_name == input_layer_name_) {
        return layers[input_layer_name_]->forward({ input }, apply_normalization);
    } else {
        std::vector<Eigen::Tensor<dco_type, 3>> inputs;
        std::vector<std::string> input_layers = layers[layer_name]->get_input_nodes();

        for (const auto& input_layer_name : input_layers) {
            inputs.push_back(this->forward(input, input_layer_name, apply_normalization));
        }

        // std::cout << "return for layer " << layer_name << std::endl;
        return layers[layer_name]->forward(inputs, apply_normalization);
    }
}

void FunctionalNetwork::print()
{
    std::cout << "------------------------------------" << std::endl;
    std::cout << "Functional Network\n" << std::endl;
    for (const auto& layer : layers) {
        std::get<1>(layer)->print();
    }
    std::cout << "------------------------------------" << std::endl;
}
nlohmann::json FunctionalNetwork::to_json()
{
    nlohmann::json layers_json;
    for (const auto& layer : layers) {
        nlohmann::json layer_json = std::get<1>(layer)->to_json();
        layers_json[std::get<0>(layer)] = layer_json;
    }

    auto j = nlohmann::json { { "type", "functional" }, { "input_layer", input_layer_name_ },
        { "output_layer", output_layer_name_ }, { "layers", layers_json } };

    return j;
}
}