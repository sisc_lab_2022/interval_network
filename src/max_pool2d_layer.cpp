// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include "interval_network/max_pool2d_layer.hpp"
#include "interval_network/activation_functions.hpp"
#include "interval_network/layer.hpp"
#include "interval_network_config.hpp"
#include <algorithm>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <unsupported/Eigen/CXX11/Tensor>

namespace interval_network {
// estimate for max function
// necessary for older dco version
// dco_type max_estimate(const dco_type& a, const dco_type& b)
// {
//     if (dco::value(a).upper() >= dco::value(b).upper() && dco::value(a).lower() >= dco::value(b).lower()) {
//         return a;
//     } else if (dco::value(a).upper() <= dco::value(b).upper() && dco::value(a).lower() <= dco::value(b).lower()) {
//         return b;
//     } else {
//         // estimation of the maximum. The same Sher used for maxpoolin2d
//         // this result is however not correct
//         Interval diff = abs(dco::value(a - b));
//         dco_type result = 0.5 * (a + b + diff);
//         return result;
//     }
// }

MaxPool2dLayer::MaxPool2dLayer(const std::string& name, const std::string& input_node, int input_rows, int input_cols,
    int input_channels, int pool_rows, int pool_cols, std::string padding, int stride_rows, int stride_cols)
    : name_(name)
    , input_node_(input_node)
    , input_rows_(input_rows)
    , input_cols_(input_cols)
    , input_channels_(input_channels)
    , pool_rows_(pool_rows)
    , pool_cols_(pool_cols)
    , stride_rows_(stride_rows)
    , stride_cols_(stride_cols)
    , forward_called_(false)
{
    if (padding == "valid") {
        padding_rows_ = 0;
        padding_cols_ = 0;
    } else if (padding == "same") {
        // yet to be tested but for conv2d the following problem occured:
        // tensorflows implementation for this case has a strange behaviour that we did not understand.
        // Therefore combining padding with stride is not allowed here. It's not a unseful thing anyways
        assert(stride_rows == 1);
        assert(stride_cols == 1);

        padding_rows_ = pool_rows - 1;
        padding_cols_ = pool_cols - 1;
    } else {
        throw std::invalid_argument("Invalid padding type");
    }

    output_rows_ = (input_rows_ - pool_rows_ + stride_rows_ + padding_rows_) / stride_rows_;
    output_cols_ = (input_cols_ - pool_cols_ + stride_cols_ + padding_cols_) / stride_cols_;
    output_channels_ = input_channels_;

    values_ = Eigen::Tensor<dco_type, 3>(output_rows_, output_cols_, output_channels_);
    values_.setConstant(dco_type(Interval(0)));
}

MaxPool2dLayer::MaxPool2dLayer(const nlohmann::json& json)
{
    assert(json.at("type") == "maxpool2d");
    name_ = json.at("name");

    std::vector<std::string> input_nodes = json.at("input_nodes").get<std::vector<std::string>>();
    assert(input_nodes.size() == 1);
    input_node_ = input_nodes[0];

    input_rows_ = json.at("input_shape")[0];
    input_cols_ = json.at("input_shape")[1];
    input_channels_ = json.at("input_shape")[2];

    pool_rows_ = json.at("pool_size")[0];
    pool_cols_ = json.at("pool_size")[1];

    stride_rows_ = json.at("strides")[0];
    stride_cols_ = json.at("strides")[1];

    std::string padding = json.at("padding");
    // Compute the shape of the output tensor
    if (padding == "valid") {
        padding_rows_ = 0;
        padding_cols_ = 0;
    } else if (padding == "same") {
        padding_rows_ = pool_rows_ - 1;
        padding_cols_ = pool_cols_ - 1;
    } else {
        throw std::invalid_argument("Invalid padding type");
    }

    output_rows_ = (input_rows_ - pool_rows_ + stride_rows_ + padding_rows_) / stride_rows_;
    output_cols_ = (input_cols_ - pool_cols_ + stride_cols_ + padding_cols_) / stride_cols_;
    output_channels_ = input_channels_;

    values_ = Eigen::Tensor<dco_type, 3>(output_rows_, output_cols_, output_channels_);
    values_.setConstant(dco_type(Interval(0)));

    forward_called_ = false;
}

Eigen::Tensor<dco_type, 3> MaxPool2dLayer::forward(
    const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization)
{
    if (forward_called_) {
        return values_;
    }

    assert(inputs.size() == 1); // only 1 input allowed
    auto input = inputs[0];

    assert(input_rows_ == input.dimension(0));
    assert(input_cols_ == input.dimension(1));
    assert(input_channels_ == input.dimension(2));

    // Add padding to the input tensor if necessary
    Eigen::Tensor<dco_type, 3> padded_input;
    if (padding_rows_ > 0 || padding_cols_ > 0) {
        padded_input = Eigen::Tensor<dco_type, 3>(
            input_rows_ + 2 * padding_rows_, input_cols_ + 2 * padding_cols_, input_channels_);
        padded_input.setConstant(dco_type(Interval(0)));

        for (int i = 0; i < input_rows_; i++) {
            for (int j = 0; j < input_cols_; j++) {
                for (int k = 0; k < input_channels_; k++) {
                    padded_input(i + padding_rows_ / 2, j + padding_cols_ / 2, k) = input(i, j, k);
                }
            }
        }
    } else {
        padded_input = input;
    }

    for (int i = 0; i < output_rows_; i++) {
        for (int j = 0; j < output_cols_; j++) {
            for (int k = 0; k < output_channels_; k++) {
                // Initialize the max for this output element with first value that will be encountered
                dco_type max_value = padded_input(i * stride_rows_, j * stride_cols_, k);
                // Loop over the rows and columns of the kernel
                for (int m = 0; m < pool_rows_; m++) {
                    for (int n = 0; n < pool_cols_; n++) {
                        max_value = dco::internal::max(
                            max_value, padded_input(i * stride_rows_ + m, j * stride_cols_ + n, k));
                    }
                }
                // Set the output element to the max
                values_(i, j, k) = max_value;
            }
        }
    }

    forward_called_ = true;

    if (apply_normalization) {
        // TODO
    }

    return values_;
}

PruningResult MaxPool2dLayer::prune_channel(int channel_idx)
{
    auto indices_back = std::vector<std::tuple<int, int, int>>();
    auto intervals_back = std::vector<Interval>();

    for (int i = 0; i < output_rows_; i++) {
        for (int j = 0; j < output_cols_; j++) {
            auto index = std::make_tuple(i, j, channel_idx);
            indices_back.push_back(index);
            intervals_back.push_back(dco::value(values_(i, j, channel_idx)));
        }
    }

    // remove obsolete values
    input_channels_ -= 1;
    output_channels_ -= 1;

    auto values_new = Eigen::Tensor<dco_type, 3>(output_rows_, output_cols_, output_channels_);

    for (int i = 0; i < channel_idx; i++) {
        for (int j = 0; j < output_rows_; j++) {
            for (int k = 0; k < output_cols_; k++) {
                values_new(j, k, i) = values_(j, k, i);
            }
        }
    }

    for (int i = channel_idx; i < output_channels_; i++) {
        for (int j = 0; j < output_rows_; j++) {
            for (int k = 0; k < output_cols_; k++) {
                values_new(j, k, i) = values_(j, k, i + 1);
            }
        }
    }

    values_ = values_new;

    return PruningResult(indices_back, intervals_back);
}

PruningResult MaxPool2dLayer::previous_nodes_removed(const PruningResult& prev_pruning_result)
{
    if (prev_pruning_result.indices.empty()) {
        return prev_pruning_result;
    }

    // set index k to first element in list if there is a first element
    int channel_to_remove = std::get<2>(prev_pruning_result.indices[0]);

    // check if exactly one channel should be removed (only possible option so far)
    // that means if every index in the input is associated with one index in the vector
    assert(prev_pruning_result.indices.size() == input_rows_ * input_cols_);
    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            auto index = std::find(prev_pruning_result.indices.begin(), prev_pruning_result.indices.end(),
                std::make_tuple(i, j, channel_to_remove));

            // if element not in list, index will point to element after last element in list
            assert(index != prev_pruning_result.indices.end());
        }
    }

    return prune_channel(channel_to_remove);
}

std::shared_ptr<Layer> MaxPool2dLayer::clone() const
{
    std::string padding;
    if (padding_rows_ == 0 && padding_cols_ == 0) {
        padding = "valid";
    } else {
        padding = "same";
    }

    auto c = std::make_shared<MaxPool2dLayer>(name_, input_node_, input_rows_, input_cols_, input_channels_, pool_rows_,
        pool_cols_, padding, stride_rows_, stride_cols_);
    c->values_ = values_;
    c->forward_called_ = forward_called_;
    return c;
}

void MaxPool2dLayer::print() const
{
    std::cout << "MaxPool2D layer (" << input_rows_ << "," << input_cols_ << "," << input_channels_ << ") -> ("
              << output_rows_ << "," << output_cols_ << "," << output_channels_ << ") " << name_ << std::endl;
    std::cout << "\tpool size: (" << pool_rows_ << "," << pool_cols_ << ")" << std::endl;
    std::cout << "\tstrides: (" << stride_rows_ << "," << stride_cols_ << ")" << std::endl;
    std::cout << "\tpadding: (" << padding_rows_ << "," << padding_cols_ << ")" << std::endl;
    std::cout << "\tinput node: " << input_node_ << std::endl;
}

nlohmann::json MaxPool2dLayer::to_json() const
{
    std::string padding;
    if (padding_rows_ == 0 || padding_cols_ == 0) {
        padding = "valid";
    } else {
        padding = "same";
    }

    std::vector<int> pool_size(2);
    pool_size[0] = pool_rows_;
    pool_size[1] = pool_cols_;

    std::vector<int> strides(2);
    strides[0] = stride_rows_;
    strides[1] = stride_cols_;

    std::vector<int> input_shape(3);
    input_shape[0] = input_rows_;
    input_shape[1] = input_cols_;
    input_shape[2] = input_channels_;

    std::vector<std::string> input_nodes = { input_node_ };

    auto j = nlohmann::json { { "name", name_ }, { "type", "maxpool2d" }, { "input_shape", input_shape },
        { "pool_size", pool_size }, { "padding", padding }, { "strides", strides }, { "input_nodes", input_nodes } };

    return j;
}
} // namespace interval_network
