// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include "interval_network/zero_padding2d_layer.hpp"
#include "interval_network/activation_functions.hpp"
#include "interval_network/layer.hpp"
#include "interval_network_config.hpp"
#include <algorithm>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <unsupported/Eigen/CXX11/Tensor>

namespace interval_network {
ZeroPadding2DLayer::ZeroPadding2DLayer(const std::string& name, const std::string& input_node, int input_rows,
    int input_cols, int input_channels, int left_padding, int right_padding, int top_padding, int bottom_padding)
    : name_(name)
    , input_node_(input_node)
    , input_rows_(input_rows)
    , input_cols_(input_cols)
    , input_channels_(input_channels)
    , left_padding_(left_padding)
    , right_padding_(right_padding)
    , top_padding_(top_padding)
    , bottom_padding_(bottom_padding)
{
    output_channels_ = input_channels_;
    output_rows_ = input_rows_ + top_padding_ + bottom_padding_;
    output_cols_ = input_cols_ + left_padding_ + right_padding_;

    forward_called_ = false;
    values_ = Eigen::Tensor<dco_type, 3>(output_rows_, output_cols_, output_channels_);
    values_.setConstant(dco_type(Interval(0)));
}

ZeroPadding2DLayer::ZeroPadding2DLayer(const nlohmann::json& json)
{
    assert(json.at("type") == "zeropadding2d");

    name_ = json.at("name");

    std::vector<std::string> input_nodes = json.at("input_nodes").get<std::vector<std::string>>();
    assert(input_nodes.size() == 1);
    input_node_ = input_nodes[0];

    input_rows_ = json.at("input_shape")[0];
    input_cols_ = json.at("input_shape")[1];
    input_channels_ = json.at("input_shape")[2];

    top_padding_ = json.at("top_padding");
    bottom_padding_ = json.at("bottom_padding");
    left_padding_ = json.at("left_padding");
    right_padding_ = json.at("right_padding");

    output_channels_ = input_channels_;
    output_rows_ = input_rows_ + top_padding_ + bottom_padding_;
    output_cols_ = input_cols_ + left_padding_ + right_padding_;

    forward_called_ = false;
    values_ = Eigen::Tensor<dco_type, 3>(output_rows_, output_cols_, output_channels_);
    values_.setConstant(dco_type(Interval(0)));
}

Eigen::Tensor<dco_type, 3> ZeroPadding2DLayer::forward(
    const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization)
{
    if (forward_called_) {
        return values_;
    }

    assert(inputs.size() == 1); // only 1 input allowed
    auto input = inputs[0];

    assert(input.dimension(0) == input_rows_);
    assert(input.dimension(1) == input_cols_);
    assert(input.dimension(2) == input_channels_);

    values_.setConstant(Interval(0));
    for (int k = 0; k < input_channels_; k++) {
        for (int i = 0; i < input_rows_; i++) {
            for (int j = 0; j < input_cols_; j++) {
                values_(i + top_padding_, j + left_padding_, k) = input(i, j, k);
            }
        }
    }

    forward_called_ = true;

    return values_;
}

PruningResult ZeroPadding2DLayer::prune_channel(int channel_idx)
{
    auto intervals_back = std::vector<Interval>();
    auto indices = std::vector<std::tuple<int, int, int>>();

    for (int i = 0; i < output_rows_; i++) {
        for (int j = 0; j < output_cols_; j++) {
            auto index = std::make_tuple(i, j, channel_idx);
            indices.push_back(index);
            intervals_back.push_back(dco::value(values_(i, j, channel_idx)));
        }
    }

    // remove obsolete values
    input_channels_ -= 1;
    output_channels_ -= 1;

    auto values_new = Eigen::Tensor<dco_type, 3>(output_rows_, output_cols_, output_channels_);

    for (int i = 0; i < channel_idx; i++) {
        for (int j = 0; j < output_rows_; j++) {
            for (int k = 0; k < output_cols_; k++) {
                values_new(j, k, i) = values_(j, k, i);
            }
        }
    }

    for (int i = channel_idx; i < output_channels_; i++) {
        for (int j = 0; j < output_rows_; j++) {
            for (int k = 0; k < output_cols_; k++) {
                values_new(j, k, i) = values_(j, k, i + 1);
            }
        }
    }

    return PruningResult(indices, intervals_back);
}

PruningResult ZeroPadding2DLayer::previous_nodes_removed(const PruningResult& prev_pruning_result)
{
    if (prev_pruning_result.indices.empty()) {
        return prev_pruning_result;
    }

    // set index k to first element in list if there is a first element
    int channel_to_remove = std::get<2>(prev_pruning_result.indices[0]);

    // check if exactly one channel should be removed (only possible option so far)
    // that means if every index in the input is associated with one index in the vector
    assert(prev_pruning_result.indices.size() == input_rows_ * input_cols_);
    for (int i = 0; i < input_rows_; i++) {
        for (int j = 0; j < input_cols_; j++) {
            auto index = std::find(prev_pruning_result.indices.begin(), prev_pruning_result.indices.end(),
                std::make_tuple(i, j, channel_to_remove));

            // if element not in list, index will point to element after last element in list
            assert(index != prev_pruning_result.indices.end());
        }
    }

    return prune_channel(channel_to_remove);
}

nlohmann::json ZeroPadding2DLayer::to_json() const
{
    std::vector<std::string> input_nodes = { input_node_ };

    std::vector<int> input_shape(3);
    input_shape[0] = input_rows_;
    input_shape[1] = input_cols_;
    input_shape[2] = input_channels_;

    auto j = nlohmann::json { { "name", name_ }, { "type", "zeropadding2d" }, { "input_nodes", input_nodes },
        { "input_shape", input_shape }, { "right_padding", right_padding_ }, { "left_padding", left_padding_ },
        { "bottom_padding", bottom_padding_ }, { "top_padding", top_padding_ } };

    return j;
}

void ZeroPadding2DLayer::print() const
{
    std::cout << "ZeroPadding2D layer (" << input_rows_ << "," << input_cols_ << "," << input_channels_ << ") -> ("
              << output_rows_ << "," << output_cols_ << "," << output_channels_ << ") " << name_ << std::endl;
    std::cout << "\tinput node: " << input_node_ << std::endl;
}

std::shared_ptr<Layer> ZeroPadding2DLayer::clone() const
{
    auto c = std::make_shared<ZeroPadding2DLayer>(name_, input_node_, input_rows_, input_cols_, input_channels_,
        left_padding_, right_padding_, top_padding_, bottom_padding_);
    c->values_ = values_;
    c->forward_called_ = forward_called_;
    return c;
}

} // namespace interval_network
