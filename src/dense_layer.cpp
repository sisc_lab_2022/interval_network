// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include "interval_network/dense_layer.hpp"
#include "interval_network/activation_functions.hpp"
#include "interval_network/layer.hpp"
#include "interval_network_config.hpp"
#include <algorithm>
#include <cassert>
#include <dco.hpp>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <string>
#include <tuple>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

namespace interval_network {

DenseLayer::DenseLayer(const std::string& name, const std::string& input_node, const Eigen::Tensor<interval_bt, 2>& w,
    const Eigen::Tensor<interval_bt, 1>& b, const std::shared_ptr<ActivationFunction>& f)
    : name_(name)
    , input_node_(input_node)
    , weights_(w)
    , biases_(b)
    , activation_(f)
    , forward_called_(false)
{
    assert(biases_.dimension(0) == weights_.dimension(0));

    significances_ = std::vector<interval_bt>(weights_.dimension(0), 0);

    values_ = Eigen::Tensor<dco_type, 3>(1, 1, weights_.dimension(0));
}

DenseLayer::DenseLayer(const nlohmann::json& json)
{
    assert(json.at("type") == "dense");

    name_ = json.at("name");

    std::vector<std::string> input_nodes = json.at("input_nodes").get<std::vector<std::string>>();
    assert(input_nodes.size() == 1);

    input_node_ = input_nodes[0];

    // weights
    weights_ = Eigen::Tensor<interval_bt, 2>(json.at("weights").size(), json.at("weights")[0].size());
    for (size_t i = 0; i < weights_.dimension(0); i++) {
        for (size_t j = 0; j < weights_.dimension(1); j++) {
            weights_(i, j) = json.at("weights")[i][j];
        }
    }

    // biases
    biases_ = Eigen::Tensor<interval_bt, 1>(json.at("biases").size());
    for (size_t i = 0; i < biases_.dimension(0); i++) {
        biases_(i) = json.at("biases")[i];
    }

    // activation function
    activation_ = activation_from_json(json.at("activation"));

    // rest
    significances_ = std::vector<interval_bt>(weights_.dimension(0), 0);
    values_ = Eigen::Tensor<dco_type, 3>(1, 1, weights_.dimension(0));
    forward_called_ = false;
}

std::shared_ptr<Layer> DenseLayer::clone() const
{
    auto activation_clone = activation_->clone();
    // are weights really being copied??
    auto c = std::make_shared<DenseLayer>(name_, input_node_, weights_, biases_, activation_clone);
    c->values_ = values_;
    c->significances_ = significances_;
    c->forward_called_ = forward_called_;
    return c;
}

Eigen::Tensor<dco_type, 3> DenseLayer::forward(
    const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization)
{
    if (forward_called_) {
        return values_;
    }

    assert(inputs.size() == 1); // only 1 input allowed
    auto input = inputs[0];

    assert(input.dimension(1) == 1); // only vector as input allowed
    assert(input.dimension(0) == 1);
    assert(input.dimension(2) == weights_.dimension(1)); // dimensions for w * inp

    int output_size = values_.dimension(2);

    // weights X input
    for (int i = 0; i < output_size; i++) {
        values_(0, 0, i) = 0.;
        for (int j = 0; j < weights_.dimension(1); j++) {
            values_(0, 0, i) += weights_(i, j) * input(0, 0, j);
        }
    }

    // values + bias
    for (int i = 0; i < output_size; i++) {
        values_(0, 0, i) += biases_(i);
    }

    // activation function
    values_ = activation_->eval(values_);
    forward_called_ = true;

    if (apply_normalization) {
        // TODO
    }

    return values_;
}

void DenseLayer::update_significance()
{
    for (int i = 0; i < values_.dimension(2); i++) {

        interval_bt width = dco::value(values_(0, 0, i)).upper() - dco::value(values_(0, 0, i)).lower();
        interval_bt abs_derivative_up = abs(dco::derivative(values_(0, 0, i)).upper());
        interval_bt abs_derivative_low = abs(dco::derivative(values_(0, 0, i)).lower());
        interval_bt abs_derivative = std::max(abs_derivative_up, abs_derivative_low);
        interval_bt significance = width * abs_derivative;
        // interval_bt significance = width;
        significances_[i] = std::max(significances_[i], significance);
    }
}

void DenseLayer::print() const
{
    std::cout << "Dense layer (1, 1, " << weights_.dimension(1) << ") -> (1, 1, " << weights_.dimension(0) << ") "
              << name_ << std::endl;
    std::cout << "\tactivation function: " << activation_->name() << std::endl;
    // std::cout << "\tsignificances: " << significances_.reshape(Eigen::array<int, 2>{{1,
    // (int)significances_.dimension(0)}}) << std::endl;
    std::cout << "\tinput node: " << input_node_ << std::endl;
}

nlohmann::json DenseLayer::to_json() const
{
    auto weights
        = std::vector<std::vector<interval_bt>>(weights_.dimension(0), std::vector<interval_bt>(weights_.dimension(1)));
    for (size_t i = 0; i < weights_.dimension(0); i++) {
        for (size_t j = 0; j < weights_.dimension(1); j++) {
            weights[i][j] = weights_(i, j);
        }
    }

    auto biases = std::vector<interval_bt>(biases_.dimension(0));
    for (size_t i = 0; i < biases_.dimension(0); i++) {
        biases[i] = biases_(i);
    }

    nlohmann::json activation = activation_->to_json();
    std::vector<std::string> input_nodes = { input_node_ };

    auto j = nlohmann::json { { "name", name_ }, { "type", "dense" }, { "weights", weights }, { "biases", biases },
        { "activation", activation }, { "input_nodes", input_nodes } };

    return j;
}

PruningResult DenseLayer::prune_channel(int channel_idx)
{
    Interval interval_back = dco::value(values_(0, 0, channel_idx));

    // size of new weight matrix
    int num_rows = weights_.dimension(0) - 1;
    int num_cols = weights_.dimension(1);

    // delete unnecessary row of weight matrix
    Eigen::Tensor<interval_bt, 2> new_weights(num_rows, num_cols);
    for (size_t i = 0; i < channel_idx; i++) {
        for (size_t j = 0; j < weights_.dimension(1); j++) {
            new_weights(i, j) = weights_(i, j);
        }
    }
    for (size_t i = channel_idx + 1; i < weights_.dimension(0); i++) {
        for (size_t j = 0; j < weights_.dimension(1); j++) {
            new_weights(i - 1, j) = weights_(i, j);
        }
    }
    weights_ = new_weights;

    // delete unnecessary biases
    Eigen::Tensor<interval_bt, 1> new_biases(num_rows);
    for (size_t i = 0; i < channel_idx; i++) {
        new_biases(i) = biases_(i);
    }
    for (size_t i = channel_idx + 1; i < biases_.dimension(0); i++) {
        new_biases(i - 1) = biases_(i);
    }
    biases_ = new_biases;

    // delete unnecessary values
    Eigen::Tensor<dco_type, 3> new_values(1, 1, num_rows);
    for (int i = 0; i < channel_idx; i++) {
        new_values(0, 0, i) = values_(0, 0, i);
    }
    for (int i = channel_idx + 1; i < values_.dimension(2); i++) {
        new_values(0, 0, i - 1) = values_(0, 0, i);
    }
    values_ = new_values;

    // delete unnecessary significances
    std::vector<interval_bt> new_significances(num_rows);
    for (size_t i = 0; i < channel_idx; i++) {
        new_significances[i] = significances_[i];
    }
    for (size_t i = channel_idx + 1; i < significances_.size(); i++) {
        new_significances[i - 1] = significances_[i];
    }
    significances_ = new_significances;

    std::vector<std::tuple<int, int, int>> indices = { std::make_tuple(0, 0, channel_idx) };
    std::vector<Interval> interval_midpoints = { interval_back };
    return PruningResult(indices, interval_midpoints);
}

void DenseLayer::previousNeuronRemoved(int idx, Interval interval)
{
    // add interval midpoint as bias
    for (size_t i = 0; i < biases_.dimension(0); i++) {
        Interval add_bias = (weights_(i, idx) * interval);
        biases_(i) = biases_(i) + (add_bias.upper() + add_bias.lower()) / 2.;
    }

    // size of new weight matrix
    int num_rows = weights_.dimension(0);
    int num_cols = weights_.dimension(1) - 1;

    // delete unnecessary column of weight matrix
    Eigen::Tensor<interval_bt, 2> new_weights(num_rows, num_cols);
    for (size_t i = 0; i < weights_.dimension(0); i++) {
        for (size_t j = 0; j < idx; j++) {
            new_weights(i, j) = weights_(i, j);
        }
    }
    for (size_t i = 0; i < weights_.dimension(0); i++) {
        for (size_t j = idx + 1; j < weights_.dimension(1); j++) {
            new_weights(i, j - 1) = weights_(i, j);
        }
    }
    weights_ = new_weights;
}

PruningResult DenseLayer::previous_nodes_removed(const PruningResult& prev_pruning_result)
{
    auto actual_indices = prev_pruning_result.indices;

    for (size_t i = 0; i < actual_indices.size(); i++) {
        assert(std::get<1>(actual_indices[i]) == 0);
        assert(std::get<0>(actual_indices[i]) == 0);

        previousNeuronRemoved(std::get<2>(actual_indices[i]), prev_pruning_result.intervals[i]);

        for (size_t j = 0; j < actual_indices.size(); j++) {
            if (std::get<2>(actual_indices[j]) > std::get<2>(actual_indices[i])) {
                actual_indices[j] = std::make_tuple(
                    std::get<0>(actual_indices[j]), std::get<1>(actual_indices[j]), std::get<2>(actual_indices[j]) - 1);
            }
        }
    }

    auto indices_back = std::vector<std::tuple<int, int, int>>();
    auto intervals_back = std::vector<Interval>();

    return PruningResult(indices_back, intervals_back);
}
} // namespace interval_network