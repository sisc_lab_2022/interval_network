// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include "interval_network/activation_functions.hpp"
#include "interval_network_config.hpp"
#include <cassert>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <unsupported/Eigen/CXX11/Tensor>

namespace interval_network {
Linear::Linear(const nlohmann::json& j) { assert(j.at("type") == "linear"); }

std::shared_ptr<ActivationFunction> Linear::clone() const
{
    auto c = std::make_shared<Linear>();
    return c;
}

nlohmann::json Linear::to_json() const
{
    nlohmann::json j = { { "type", "linear" } };
    return j;
}

Sigmoid::Sigmoid(const nlohmann::json& j) { assert(j.at("type") == "sigmoid"); }

std::shared_ptr<ActivationFunction> Sigmoid::clone() const
{
    auto c = std::make_shared<Sigmoid>();
    return c;
}

Eigen::Tensor<dco_type, 3> Sigmoid::eval(const Eigen::Tensor<dco_type, 3>& input) const
{
    Eigen::Tensor<dco_type, 3> res(input.dimension(0), input.dimension(1), input.dimension(2));

    for (int i = 0; i < input.dimension(0); i++) {
        for (int j = 0; j < input.dimension(1); j++) {
            for (int k = 0; k < input.dimension(2); k++) {
                // dco_type res = exp(input) / (exp(input) + Interval(1.)); //gives large errors
                res(i, j, k) = Interval(1., 1.) / (Interval(1., 1.) + exp(-input(i, j, k)));
            }
        }
    }

    return res;
}

nlohmann::json Sigmoid::to_json() const
{
    nlohmann::json j = { { "type", "sigmoid" } };
    return j;
}

Relu::Relu(const nlohmann::json& j) { assert(j.at("type") == "relu"); }

Eigen::Tensor<dco_type, 3> Relu::eval(const Eigen::Tensor<dco_type, 3>& input) const
{
    Eigen::Tensor<dco_type, 3> res(input.dimension(0), input.dimension(1), input.dimension(2));

    for (int i = 0; i < input.dimension(0); i++) {
        for (int j = 0; j < input.dimension(1); j++) {
            for (int k = 0; k < input.dimension(2); k++) {
                // for older dco version
                // if (dco::value(input(i, j, k)).upper() <= 0) {
                //     res(i, j, k) = Interval(0) * input(i, j, k);
                // } else if (dco::value(input(i, j, k)).lower() <= 0) {
                //     Interval temp(0, 1);
                //     dco::value(res(i, j, k)).set(0, dco::value(input(i, j, k)).upper());
                //     res(i, j, k) *= temp;
                // } else {
                //     res(i, j, k) = input(i, j, k);
                // }

                // for newer version
                res(i, j, k) = dco::internal::relu(input(i, j, k));
            }
        }
    }

    return res;
}

nlohmann::json Relu::to_json() const
{
    nlohmann::json j = { { "type", "relu" } };
    return j;
}

std::shared_ptr<ActivationFunction> Relu::clone() const
{
    auto c = std::make_shared<Relu>();
    return c;
}

} // namespace interval_network