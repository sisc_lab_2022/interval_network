cmake_minimum_required(VERSION 3.15.0)
project(IntervalNetwork VERSION 0.1.0 LANGUAGES CXX)

SET( CMAKE_CXX_STANDARD 11 )
SET( CMAKE_CXX_STANDARD_REQUIRED ON )

if(NOT INTERVAL_NETWORK_INTERVAL_BASETYPE)
    SET(INTERVAL_NETWORK_INTERVAL_BASETYPE double)
endif()

configure_file(interval_network_config.hpp.in interval_network_config.hpp)

if(CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)

    set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")
    set_property(GLOBAL PROPERTY USE_FOLDERS ON)
    include(CTest)

endif()


find_package(NAG_dco_cpp REQUIRED)
find_package(Boost REQUIRED)
find_package (Eigen3 3.4 REQUIRED)
find_package(nlohmann_json 3 REQUIRED)

# add_compile_options(-DDCO_CHUNK_TAPE)


add_subdirectory(src)


if(CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
    
    add_subdirectory(examples)
    add_subdirectory(test)

endif()

# set(CPACK_PROJECT_NAME ${PROJECT_NAME})
# set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
# include(CPack)
