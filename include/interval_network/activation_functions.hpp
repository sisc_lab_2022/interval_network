// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#pragma once

#include "interval_network_config.hpp"
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <unsupported/Eigen/CXX11/Tensor>

namespace interval_network {

class ActivationFunction {
public:
    virtual Eigen::Tensor<dco_type, 3> eval(const Eigen::Tensor<dco_type, 3>& input) const = 0;
    virtual std::string name() const = 0;
    virtual nlohmann::json to_json() const = 0;
    virtual std::shared_ptr<ActivationFunction> clone() const = 0;
};

class Linear : public ActivationFunction {
public:
    Linear() { }
    Linear(const nlohmann::json& j);
    Eigen::Tensor<dco_type, 3> eval(const Eigen::Tensor<dco_type, 3>& input) const override { return input; }
    std::string name() const override { return "Linear"; }
    nlohmann::json to_json() const override;
    std::shared_ptr<ActivationFunction> clone() const override;
};

class Sigmoid : public ActivationFunction {
public:
    Sigmoid() { }
    Sigmoid(const nlohmann::json& j);
    Eigen::Tensor<dco_type, 3> eval(const Eigen::Tensor<dco_type, 3>& input) const override;
    std::string name() const override { return "Sigmoid"; }
    nlohmann::json to_json() const override;
    std::shared_ptr<ActivationFunction> clone() const override;
};

class Relu : public ActivationFunction {
public:
    Relu() { }
    Relu(const nlohmann::json& j);
    Eigen::Tensor<dco_type, 3> eval(const Eigen::Tensor<dco_type, 3>& input) const override;
    std::string name() const override { return "Relu"; }
    nlohmann::json to_json() const override;
    std::shared_ptr<ActivationFunction> clone() const override;
};

inline std::shared_ptr<ActivationFunction> activation_from_json(const nlohmann::json& json)
{
    std::shared_ptr<ActivationFunction> activation;

    if (json.at("type") == "linear") {
        activation = std::make_shared<Linear>(json);
    } else if (json.at("type") == "relu") {
        activation = std::make_shared<Relu>(json);
    } else if (json.at("type") == "sigmoid") {
        activation = std::make_shared<Sigmoid>(json);
    } else {
        throw std::invalid_argument("Unknown activation function " + (std::string)json.at("type"));
    }

    return activation;
}

} // namespace interval_network