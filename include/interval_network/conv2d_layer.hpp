// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce// Copyright @stce

#pragma once

#include "activation_functions.hpp"
#include "interval_network_config.hpp"
#include "layer.hpp"
#include <algorithm>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <tuple>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

namespace interval_network {

class Conv2dLayer : public Layer {
public:
    Conv2dLayer(const std::string& name, const std::string& input_node, const Eigen::Tensor<interval_bt, 4>& kernel,
        int input_rows, int input_cols, std::string padding, int stride_rows, int stride_cols,
        const Eigen::Tensor<interval_bt, 1>& biases, const std::shared_ptr<ActivationFunction>& f);
    Conv2dLayer(const nlohmann::json& json);

    std::string get_name() const override { return name_; }
    std::vector<std::string> get_input_nodes() const override { return { input_node_ }; }
    const Eigen::Tensor<dco_type, 3>& get_values() const override { return values_; }
    bool get_forward_called() const override { return forward_called_; }
    void reset_forward_called() override { forward_called_ = false; }

    Eigen::Tensor<dco_type, 3> forward(
        const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization) override;
    void update_significance() override;
    int prunable_elements() const override { return output_channels_; }
    std::vector<interval_bt> get_significances() const override;
    PruningResult prune_channel(int channel_idx) override;
    PruningResult previous_nodes_removed(const PruningResult& prev_pruning_result) override;
    nlohmann::json to_json() const override;
    void print() const override;
    std::shared_ptr<Layer> clone() const override;

    void previousChannelRemoved(int channel_index, const std::vector<Interval>& intervals);

private:
    std::string name_;
    std::string input_node_;
    // Kernel tensor
    Eigen::Tensor<interval_bt, 4> kernel_;
    // shape of input and output
    int input_rows_;
    int input_cols_;
    int input_channels_;

    int output_rows_;
    int output_cols_;
    int output_channels_;
    // Padding size
    int padding_rows_;
    int padding_cols_;
    // strides
    int stride_rows_;
    int stride_cols_;
    // rest
    Eigen::Tensor<dco_type, 3> values_;
    Eigen::Tensor<interval_bt, 1> biases_;
    std::shared_ptr<ActivationFunction> activation_;
    Eigen::Tensor<interval_bt, 3> significances_;
    bool forward_called_; // was the forward method called
};

} // namespace interval_network