// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce
#pragma once

#include "interval_network_config.hpp"
#include "pruning_result.hpp"
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <tuple>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

namespace interval_network {

class Layer {
public:
    Layer(const Layer& other) = delete;
    Layer() = default;

    virtual std::string get_name() const = 0;
    virtual const Eigen::Tensor<dco_type, 3>& get_values() const = 0;
    virtual std::vector<std::string> get_input_nodes() const = 0;
    virtual bool get_forward_called() const = 0;
    virtual void reset_forward_called() = 0;
    virtual Eigen::Tensor<dco_type, 3> forward(
        const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization)
        = 0;
    virtual void update_significance() = 0;
    virtual int prunable_elements() const = 0;
    virtual std::vector<interval_bt> get_significances() const = 0;
    virtual PruningResult prune_channel(int channel_idx) = 0;
    virtual PruningResult previous_nodes_removed(const PruningResult& prev_pruning_result) = 0;
    virtual nlohmann::json to_json() const = 0;
    virtual void print() const = 0;

    // basically instead of copy constructor
    virtual std::shared_ptr<Layer> clone() const = 0;
};
}