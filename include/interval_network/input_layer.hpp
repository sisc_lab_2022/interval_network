// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#pragma once

#include "activation_functions.hpp"
#include "interval_network_config.hpp"
#include "layer.hpp"
#include <algorithm>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <tuple>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

namespace interval_network {

class InputLayer : public Layer {
public:
    InputLayer(const std::string& name, int input_rows, int input_cols, int input_channels)
        : name_(name)
        , input_rows_(input_rows)
        , input_cols_(input_cols)
        , input_channels_(input_channels)
    {
    }

    InputLayer(const nlohmann::json& json)
    {
        assert(json.at("type") == "input");
        name_ = json.at("name");
        input_rows_ = json.at("input_shape")[0];
        input_cols_ = json.at("input_shape")[1];
        input_channels_ = json.at("input_shape")[2];
    }

    std::string get_name() const override { return name_; }
    std::vector<std::string> get_input_nodes() const override { return std::vector<std::string>(); }

    virtual const Eigen::Tensor<dco_type, 3>& get_values() const override
    {
        throw std::logic_error("InputLayer does not store values yet");
    }

    Eigen::Tensor<dco_type, 3> forward(
        const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization) override
    {
        assert(inputs.size() == 1); // only 1 input allowed
        auto input = inputs[0];

        return input;
    }

    void update_significance() override { }
    int prunable_elements() const override { return 0; } // should be more, but not implemented yet
    std::vector<interval_bt> get_significances() const override { throw std::logic_error("Not implemented yet"); }
    bool get_forward_called() const override { return false; }
    void reset_forward_called() override { }

    PruningResult prune_channel(int channel_idx) override { throw std::logic_error("Not implemented yet"); }

    PruningResult previous_nodes_removed(const PruningResult& prev_pruning_result) override
    {
        throw std::logic_error("The input Layer is always the first layer in a network");
    }

    nlohmann::json to_json() const override
    {
        std::vector<int> input_shape(3);
        input_shape[0] = input_rows_;
        input_shape[1] = input_cols_;
        input_shape[2] = input_channels_;
        std::vector<std::string> input_nodes(0);

        nlohmann::json j = { { "name", name_ }, { "type", "input" }, { "input_shape", input_shape },
            { "input_nodes", input_nodes } };

        return j;
    }

    void print() const override
    {
        std::cout << "Input layer (" << input_rows_ << "," << input_cols_ << "," << input_channels_ << ") -> ("
                  << input_rows_ << "," << input_cols_ << "," << input_channels_ << ") " << name_ << std::endl;
    }
    std::shared_ptr<Layer> clone() const override
    {
        std::shared_ptr<Layer> c = std::make_shared<InputLayer>(name_, input_rows_, input_cols_, input_channels_);
        return c;
    }

private:
    std::string name_;
    int input_rows_;
    int input_cols_;
    int input_channels_;
};

} // namespace interval_network