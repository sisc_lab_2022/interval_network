// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#pragma once

#include "activation_functions.hpp"
#include "interval_network_config.hpp"
#include "layer.hpp"
#include <algorithm>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <tuple>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

namespace interval_network {

class BatchnormalizationLayer : public Layer {
public:
    BatchnormalizationLayer(const std::string& name, const std::string& input_node, int input_rows, int input_cols,
        int input_channels, interval_bt momentum, interval_bt epsilon, const Eigen::Tensor<interval_bt, 1>& gamma,
        const Eigen::Tensor<interval_bt, 1>& beta, const Eigen::Tensor<interval_bt, 1>& mean,
        const Eigen::Tensor<interval_bt, 1>& variance);
    BatchnormalizationLayer(const nlohmann::json& json);

    std::string get_name() const override { return name_; }
    std::vector<std::string> get_input_nodes() const override { return { input_node_ }; }
    const Eigen::Tensor<dco_type, 3>& get_values() const override { return values_; }
    bool get_forward_called() const override { return forward_called_; }
    void reset_forward_called() override { forward_called_ = false; }

    Eigen::Tensor<dco_type, 3> forward(
        const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization) override;
    void update_significance() override { }
    int prunable_elements() const override { return 0; }
    std::vector<interval_bt> get_significances() const override { throw std::logic_error("Not implemented yet"); }
    PruningResult prune_channel(int channel_idx) override;
    PruningResult previous_nodes_removed(const PruningResult& prev_pruning_result) override;
    nlohmann::json to_json() const override;
    void print() const override;
    std::shared_ptr<Layer> clone() const override;

private:
    std::string name_;
    std::string input_node_;

    int input_rows_;
    int input_cols_;
    int input_channels_;

    interval_bt momentum_;
    interval_bt epsilon_;
    Eigen::Tensor<interval_bt, 1> gamma_;
    Eigen::Tensor<interval_bt, 1> beta_;
    Eigen::Tensor<interval_bt, 1> mean_;
    Eigen::Tensor<interval_bt, 1> variance_;

    Eigen::Tensor<dco_type, 3> values_;
    bool forward_called_; // was the forward method called
};

} // namespace interval_network