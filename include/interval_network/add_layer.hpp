// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#pragma once

#include "activation_functions.hpp"
#include "interval_network_config.hpp"
#include "layer.hpp"
#include <algorithm>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <tuple>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

namespace interval_network {

class AddLayer : public Layer {
public:
    AddLayer(const std::string& name, const std::vector<std::string>& input_layers, int input_rows, int input_cols,
        int input_channels);
    AddLayer(const nlohmann::json& json);

    std::string get_name() const override { return name_; }
    std::vector<std::string> get_input_nodes() const override { return input_nodes_; }
    const Eigen::Tensor<dco_type, 3>& get_values() const override { return values_; }
    bool get_forward_called() const override { return forward_called_; }
    void reset_forward_called() override { forward_called_ = false; }

    Eigen::Tensor<dco_type, 3> forward(
        const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization) override;
    void update_significance() override;
    int prunable_elements() const override { return 0; }
    std::vector<interval_bt> get_significances() const override;

    PruningResult prune_channel(int channel_idx) override;
    PruningResult previous_nodes_removed(const PruningResult& prev_pruning_result) override;

    nlohmann::json to_json() const override;
    void print() const override;
    std::shared_ptr<Layer> clone() const override;

private:
    std::string name_;
    std::vector<std::string> input_nodes_;

    int input_rows_;
    int input_cols_;
    int input_channels_;

    Eigen::Tensor<dco_type, 3> values_;
    Eigen::Tensor<interval_bt, 3> significances_;
    bool forward_called_; // was the forward method called
};

} // namespace interval_network