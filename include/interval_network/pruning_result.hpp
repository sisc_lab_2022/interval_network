// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#pragma once

#include "interval_network_config.hpp"
#include <tuple>
#include <vector>

namespace interval_network {
struct PruningResult {
    std::vector<std::tuple<int, int, int>> indices;
    std::vector<Interval> intervals;

    PruningResult(const std::vector<std::tuple<int, int, int>>& indices, const std::vector<Interval>& intervals)
        : indices(indices)
        , intervals(intervals)
    {
    }
};
} // namespace interval_network
