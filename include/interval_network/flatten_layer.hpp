// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#pragma once

#include "interval_network_config.hpp"
#include "layer.hpp"
#include <cassert>
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <string>
#include <tuple>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

namespace interval_network {

class FlattenLayer : public Layer {
public:
    FlattenLayer(
        const std::string& name, const std::string& input_node, int input_rows, int input_cols, int input_channels);
    FlattenLayer(const nlohmann::json& json);

    std::string get_name() const override { return name_; }
    std::vector<std::string> get_input_nodes() const override { return { input_node_ }; }
    const Eigen::Tensor<dco_type, 3>& get_values() const override
    {
        throw std::logic_error("FlattenLayer does not store values yet");
    }
    bool get_forward_called() const override { return false; }
    void reset_forward_called() override { }

    std::shared_ptr<Layer> clone() const override;
    Eigen::Tensor<dco_type, 3> forward(
        const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization) override;
    void update_significance() override { }
    int prunable_elements() const override { return 0; }

    std::vector<interval_bt> get_significances() const override
    {
        throw std::logic_error("A flatten layer does not have significances");
    }

    PruningResult prune_channel(int channel_idx) override { throw std::logic_error("Flatten layer can't be pruned"); }

    PruningResult previous_nodes_removed(const PruningResult& prev_pruning_result) override;
    void print() const override;
    nlohmann::json to_json() const override;

private:
    std::string name_;
    std::string input_node_;
    int input_rows_;
    int input_cols_;
    int input_channels_;
};
} // namespace interval_network
