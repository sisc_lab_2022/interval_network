// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#pragma once

#include "activation_functions.hpp"
#include "interval_network_config.hpp"
#include "layer.hpp"
#include <algorithm>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <unsupported/Eigen/CXX11/Tensor>

namespace interval_network {

class MaxPool2dLayer : public Layer {
public:
    MaxPool2dLayer(const std::string& name, const std::string& input_node, int input_rows, int input_cols,
        int input_channels, int pool_rows, int pool_cols, std::string padding, int stride_rows, int stride_cols);
    MaxPool2dLayer(const nlohmann::json& json);

    std::shared_ptr<Layer> clone() const override;

    std::string get_name() const override { return name_; }
    std::vector<std::string> get_input_nodes() const override { return { input_node_ }; }
    const Eigen::Tensor<dco_type, 3>& get_values() const override { return values_; }
    bool get_forward_called() const override { return forward_called_; }
    void reset_forward_called() override { forward_called_ = false; }

    Eigen::Tensor<dco_type, 3> forward(
        const std::vector<Eigen::Tensor<dco_type, 3>>& inputs, bool apply_normalization) override;
    void update_significance() override { }
    int prunable_elements() const override { return 0; }
    std::vector<interval_bt> get_significances() const override { throw std::logic_error("Not implemented yet"); }
    PruningResult prune_channel(int channel_idx) override;
    PruningResult previous_nodes_removed(const PruningResult& prev_pruning_result) override;
    void print() const override;
    nlohmann::json to_json() const override;

private:
    std::string name_;
    std::string input_node_;

    int input_rows_;
    int input_cols_;
    int input_channels_;

    int pool_rows_;
    int pool_cols_;

    int padding_rows_;
    int padding_cols_;

    int stride_rows_;
    int stride_cols_;

    int output_rows_;
    int output_cols_;
    int output_channels_;

    Eigen::Tensor<dco_type, 3> values_;
    bool forward_called_; // was the forward method called
};
} // namespace interval_network
