// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#pragma once

#include "interval_network_config.hpp"
#include "layer.hpp"
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <unordered_map>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

namespace interval_network {

class FunctionalNetwork {
public:
    std::unordered_map<std::string, std::shared_ptr<Layer>> layers;

    // for future: Use every layer's clone method to perform the copy
    // for now copying disabled
    FunctionalNetwork(const FunctionalNetwork&) = delete;
    FunctionalNetwork(const nlohmann::json& j);
    Eigen::Tensor<dco_type, 3> forward(const Eigen::Tensor<dco_type, 3>& input, bool apply_normalization);
    void update_significances();
    void print();
    nlohmann::json to_json();

    std::string get_input_layer_name() const { return input_layer_name_; }
    std::string get_output_layer_name() const { return output_layer_name_; }

private:
    std::string input_layer_name_;
    std::string output_layer_name_;

    Eigen::Tensor<dco_type, 3> forward(
        const Eigen::Tensor<dco_type, 3>& input, const std::string& layer_name, bool apply_normalization);
};
}