// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include <cassert>
#include <dco.hpp>
#include <fstream>
#include <interval_network/functional_network.hpp>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <vector>

using namespace interval_network;

// reads and writes a network from/to json
int main(int argc, char** argv)
{
    assert(argc == 2);

    std::ifstream f(argv[1]);
    nlohmann::json network_json = nlohmann::json::parse(f);
    f.close();

    FunctionalNetwork network(network_json);

    nlohmann::json network_back = network.to_json();

    std::ofstream o("network_back.json");
    o << std::setw(2) << network_back << std::endl;
    o.close();

    std::ifstream f2("network_back.json");
    nlohmann::json network_read_again_json = nlohmann::json::parse(f2);
    f2.close();

    assert(network_json == network_read_again_json);
}
