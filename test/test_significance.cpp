// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include "interval_network/functional_network.hpp"
#include "interval_network_config.hpp"
#include <Eigen/Dense> // must be included after interval_network, otherwise verison mismatch
#include <cassert>
#include <cstdio>
#include <dco.hpp>
#include <fstream>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <vector>

using namespace interval_network;
using dco_ttype = dco_mode::tape_t;

// test the significance calculation with one specific network
int main(int argc, char** argv)
{
    assert(argc == 2);

    std::ifstream f(argv[1]);
    nlohmann::json network_json = nlohmann::json::parse(f);
    f.close();

    FunctionalNetwork network(network_json);

    dco_mode::global_tape = dco_mode::tape_t::create();

    // input + register input
    // 28 x 28 matrix with intervals from 0 to 1
    Eigen::Tensor<dco_type, 3> inp(28, 28, 1);
    inp.setConstant(Interval(0, 1));
    for (int i = 0; i < inp.dimension(0); i++) {
        for (int j = 0; j < inp.dimension(1); j++) {
            for (int k = 0; k < inp.dimension(2); k++) {
                dco_mode::global_tape->register_variable(inp(i, j, k));
            }
        }
    }

    // forward pass
    Eigen::Tensor<dco_type, 3> res = network.forward(inp, false);
    // std::cout << "restults from the forward pass:\n" << res << std::endl;

    // register output
    for (int i = 0; i < res.dimension(0); i++) {
        for (int j = 0; j < res.dimension(1); j++) {
            for (int k = 0; k < res.dimension(2); k++) {
                dco_mode::global_tape->register_output_variable(res(i, j, k));
            }
        }
    }

    // backward pass
    for (int i = 0; i < res.dimension(0); i++) {
        for (int j = 0; j < res.dimension(1); j++) {
            for (int k = 0; k < res.dimension(2); k++) {
                // set only one direction to 1
                dco::derivative(res(i, j, k)) = 1;

                dco_mode::global_tape->interpret_adjoint();
                network.update_significances();
                dco_mode::global_tape->zero_adjoints();
                std::cout << "\nNetwork after seeding for output node " << (i + 1) * (j + 1) * (k + 1) << ":"
                          << std::endl;
                network.print();
            }
        }
    }

    dco_ttype::remove(dco_mode::global_tape);

    auto significances = network.layers["dense"]->get_significances();

    Eigen::Matrix<interval_bt, Eigen::Dynamic, Eigen::Dynamic> true_significances { { 0.484233 }, { 0.449657 },
        { 0.434372 }, { 0.441608 }, { 0.736919 }, { 0.522695 }, { 0.584983 }, { 1.01505 }, { 0.592357 }, { 0.57539 },
        { 1.14918 }, { 0.72214 }, { 0.535924 }, { 0.701312 }, { 0.645811 }, { 0.712259 }, { 0.633225 }, { 0.366561 },
        { 0.556821 }, { 0.659936 }, { 0.575043 }, { 0.517623 }, { 0.56155 }, { 0.794662 }, { 0.59363 }, { 0.420645 },
        { 0.755249 }, { 0.447358 }, { 0.575713 }, { 0.669537 }, { 0.392908 }, { 0.523226 }, { 0.450034 }, { 0.810488 },
        { 0.416154 }, { 0.420433 }, { 0.431037 }, { 0.621483 }, { 0.596842 }, { 0.488043 }, { 0.68369 }, { 0.641126 },
        { 0.46722 }, { 0.886093 }, { 0.592585 }, { 0.544713 }, { 0.456256 }, { 0.634228 }, { 0.689639 }, { 0.473225 },
        { 0.430105 }, { 0.631232 }, { 0.54007 }, { 0.430474 }, { 0.642883 }, { 0.465691 }, { 0.547587 }, { 0.502021 },
        { 0.898948 }, { 0.483711 }, { 1.05139 }, { 0.524662 }, { 0.52705 }, { 0.436173 }, { 0.588631 }, { 0.373947 },
        { 0.5457 }, { 0.611447 }, { 0.435716 }, { 0.735077 }, { 0.711292 }, { 0.418065 }, { 0.398437 }, { 0.649983 },
        { 0.461718 }, { 0.567159 }, { 0.577961 }, { 0.648026 }, { 0.554209 }, { 0.417985 }, { 0.629762 }, { 0.494968 },
        { 0.544918 }, { 0.759964 }, { 0.608869 }, { 0.581886 }, { 0.864458 }, { 0.94216 }, { 0.683337 }, { 0.534174 },
        { 0.351445 }, { 0.472825 }, { 0.445327 }, { 0.964578 }, { 0.593604 }, { 0.648821 }, { 0.935477 }, { 0.62253 },
        { 0.600879 }, { 0.599221 }, { 0.469156 }, { 0.463211 }, { 0.570589 }, { 0.600397 }, { 0.579609 }, { 0.413713 },
        { 0.558135 }, { 0.649761 }, { 0.717478 }, { 0.54932 }, { 0.568788 }, { 0.54906 }, { 0.612482 }, { 0.69469 },
        { 0.468419 }, { 0.79947 }, { 0.46135 }, { 0.485999 }, { 0.51842 }, { 0.61662 }, { 0.556455 }, { 0.413215 },
        { 0.470913 }, { 0.868843 }, { 0.477599 }, { 0.794774 }, { 0.465203 }, { 0.477109 } };

    for (size_t i = 0; i < significances.size(); i++) {
        assert(abs(significances[i] - true_significances(i, 0)) < 1e-5);
    }
}
