// created within the SiSc Lab project 2022/23 by Jonas Bünning, Mohammad Ghanem, Lin Gu and Abdus Hashmy
// Copyright @stce

#define DCO_DISABLE_AVX2_WARNING
#define DCO_AUTO_SUPPORT

#include <cassert>
#include <dco.hpp>
#include <fstream>
#include <interval_network/functional_network.hpp>
#include <interval_network_config.hpp>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <vector>

using namespace interval_network;

// does forward pass on network and checks output
int main(int argc, char** argv)
{
    assert(argc == 3);

    std::ifstream f(argv[1]);
    nlohmann::json network_json = nlohmann::json::parse(f);
    f.close();

    FunctionalNetwork network(network_json);

    std::ifstream f2(argv[2]);
    nlohmann::json inOut_json = nlohmann::json::parse(f2);
    nlohmann::json input_json = inOut_json.at("input");
    nlohmann::json output_json = inOut_json.at("output");
    f2.close();

    Eigen::Tensor<dco_type, 3> input(input_json.size(), input_json[0].size(), 1);
    for (size_t i = 0; i < input.dimension(0); i++) {
        for (size_t j = 0; j < input.dimension(1); j++) {
            input(i, j, 0) = dco_type(Interval(input_json[i][j]));
        }
    }

    Eigen::Tensor<dco_type, 3> output(output_json.size(), output_json[0].size(), 1);
    for (size_t i = 0; i < output.dimension(0); i++) {
        for (size_t j = 0; j < output.dimension(1); j++) {
            output(i, j, 0) = dco_type(Interval(output_json[i][j]));
        }
    }

    Eigen::Tensor<dco_type, 3> res_forward = network.forward(input, false);
    for (int i = 0; i < output.dimension(0); i++) {
        for (int j = 0; j < output.dimension(1); j++) {
            assert(abs(dco::value(res_forward(0, 0, i)) - output(i, j, 0)) < 1e-5);
        }
    }
}
